[![Build Status](https://codefirst.iut.uca.fr/api/badges/jade.van_brabandt/3.01-QCM_MuscuMaths/status.svg)](https://codefirst.iut.uca.fr/jade.van_brabandt/3.01-QCM_MuscuMaths)
[![Quality Gate Status](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=MuscuMaths&metric=alert_status&token=fe8bd898783179dbdebc38d057aee2965a7592c7)](https://codefirst.iut.uca.fr/sonar/dashboard?id=MuscuMaths)
[![Bugs](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=MuscuMaths&metric=bugs&token=fe8bd898783179dbdebc38d057aee2965a7592c7)](https://codefirst.iut.uca.fr/sonar/dashboard?id=MuscuMaths)
[![Code Smells](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=MuscuMaths&metric=code_smells&token=fe8bd898783179dbdebc38d057aee2965a7592c7)](https://codefirst.iut.uca.fr/sonar/dashboard?id=MuscuMaths)
[![Coverage](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=MuscuMaths&metric=coverage&token=fe8bd898783179dbdebc38d057aee2965a7592c7)](https://codefirst.iut.uca.fr/sonar/dashboard?id=MuscuMaths)  
[![Duplicated Lines (%&token=fe8bd898783179dbdebc38d057aee2965a7592c7)](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=MuscuMaths&metric=duplicated_lines_density&token=fe8bd898783179dbdebc38d057aee2965a7592c7)](https://codefirst.iut.uca.fr/sonar/dashboard?id=MuscuMaths)
[![Lines of Code](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=MuscuMaths&metric=ncloc&token=fe8bd898783179dbdebc38d057aee2965a7592c7)](https://codefirst.iut.uca.fr/sonar/dashboard?id=MuscuMaths)
[![Maintainability Rating](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=MuscuMaths&metric=sqale_rating&token=fe8bd898783179dbdebc38d057aee2965a7592c7)](https://codefirst.iut.uca.fr/sonar/dashboard?id=MuscuMaths)
[![Reliability Rating](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=MuscuMaths&metric=reliability_rating&token=fe8bd898783179dbdebc38d057aee2965a7592c7)](https://codefirst.iut.uca.fr/sonar/dashboard?id=MuscuMaths)  
[![Security Rating](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=MuscuMaths&metric=security_rating&token=fe8bd898783179dbdebc38d057aee2965a7592c7)](https://codefirst.iut.uca.fr/sonar/dashboard?id=MuscuMaths)
[![Technical Debt](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=MuscuMaths&metric=sqale_index&token=fe8bd898783179dbdebc38d057aee2965a7592c7)](https://codefirst.iut.uca.fr/sonar/dashboard?id=MuscuMaths)
[![Vulnerabilities](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=MuscuMaths&metric=vulnerabilities&token=fe8bd898783179dbdebc38d057aee2965a7592c7)](https://codefirst.iut.uca.fr/sonar/dashboard?id=MuscuMaths)  

# 3.01-QCM_MuscuMaths (Maths'Educ)

LogicSys Digital

Application de QCM maths pour la nouvelle matière de 1ère année Maths Muscu

Convention de nommage des commits :

    build : changements qui affectent le système de build ou des dépendances externes (npm, make…)
    ci : changements concernant les fichiers et scripts d'intégration ou de configuration (Travis, Ansible, BrowserStack…)
    feat : ajout d'une nouvelle fonctionnalité
    fix : correction d'un bug
    perf : amélioration des performances
    refactor : modification qui n'apporte ni nouvelle fonctionalité ni d'amélioration de performances (Lisibilité du code ou principe SOLID par exemple)
    style : changement qui n'apporte aucune altération fonctionnelle ou sémantique (indentation, mise en forme, ajout d'espace, renommage d'une variable…)
    docs : rédaction ou mise à jour de la documentation
    test : ajout ou modification de tests

    Exemple :
    feat : ajout de la possibilité de se connecter
    perf : optimisation de requête SQL
    ...Etc...

## Buts et Objectifs du Projet

Le projet Maths'Educ vise à fournir une plateforme d'évaluation basée sur des questions à choix multiples pour la nouvelle matière de 1ère année intitulée "Maths Muscu". L'objectif principal est de créer un outil interactif qui permettra aux étudiants de tester leurs connaissances et de renforcer leur compréhension des concepts mathématiques.

Les principaux objectifs incluent :
- Développer une interface conviviale pour les QCM en mathématiques.
- Intégrer des fonctionnalités de suivi des progrès pour que les étudiants puissent mesurer leur compréhension au fil du temps.
- Assurer la compatibilité avec différents dispositifs et navigateurs pour une accessibilité optimale.

## Le backoffice

Une partie administrateur est disponible pour gérer l'ajout/la suppression des questions, des chapitres, des administrateurs et des joueurs.
Un export/import des questions au format csv est disponible.
Nous avons créé un composant complexe pour afficher les détails des questions.

Le blazor, utilisant une api, un fichier de configuration API.cs est nécessaire (n'hésitez à nous le demander).

## Les Parties Prenantes et leurs Rôles

1. **GUITARD Maxence** - Développeur polyvalent
   - Spécialisé dans la partie des utilisateurs (connexion, stockage des scores,ect..).
   - Développeur JavaScript pour la gestion du chronomètre dans les QCM.

2. **VAN BRABRANDT Jade** - Développeuse Front-End
   - Responsable de la partie jeux, incluant les questions, réponses, et le contrôle des données de formulaire.
   - Spécialisée dans la vérification des failles de sécurité dans les formulaires pour assurer une protection adéquate.

3. **DUCOURTHIAL Jérémy** - Développeur Back-End
   - En charge du développement côté serveur, particulièrement sur la gestion de la base de données.
   - Spécialisé dans l'architecture MVC (Modèle-Vue-Contrôleur) en PHP pour assurer une structure logique et organisée côté serveur.

4. **CALATAYUD Yvan** - Développeur Front-End
   - Concentré sur la partie Front-End avec une expertise particulière en Bootstrap et Twig.
   - Impliqué dans l'optimisation de l'interface utilisateur pour une expérience utilisateur (UX) fluide.

5. **NORTIER Damien** - Rédacteur de Questions en Base de Données
   - Responsable de la rédaction et de la création des questionnaires de maths pour alimenter la base de données.
   - Collaborateur actif dans l'élaboration des contenus mathématiques pertinents pour les QCM.

Chaque membre de l'équipe possède des compétences en développement Front-end et Back-end, mais chacun s'est spécialisé dans des domaines particuliers du projet pour assurer une collaboration efficace et une réalisation réussie de Maths'Educ.

Ce projet suivra les conventions de nommage des commits mentionnées précédemment pour assurer une gestion efficace du développement et de la maintenance.

Nécessite un fichier Config_DB.php pour fonctionner, n'hésitez pas à nous le demander.

## API ASP.NET

   Concernant cette partie, vous trouverez toute la documentation dans le Readme.md de la branche **API** (branche sur laquelle a été fait cette partie)

par GUITARD Maxence, VAN BRABRANDT Jade, DUCOURTHIAL Jérémy, CALATAYUD Yvan, NORTIER Damien