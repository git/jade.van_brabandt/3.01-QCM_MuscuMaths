﻿namespace Blazor.ViewClasses;

public class Player
{
    public int Id { get;  set; }
    public string Nickname { get;  set; }
    public string HashedPassword { get; set; }
    public int total_players { get; set; }
}
