﻿namespace Blazor.ViewClasses;

public class Administrator
{
    public int Id { get; set; }
    public string Username { get; set; }
    public string HashedPassword { get; set; }
    public int total_administrators { get; set; }

}
