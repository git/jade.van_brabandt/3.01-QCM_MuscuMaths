﻿using System.Text.Json.Serialization;

namespace Blazor.ViewClasses;

public class Question
{
    [JsonPropertyName("q_id")]
    public int Id { get; set; }

    [JsonPropertyName("q_content")]
    public string Content { get; set; }

    public int Difficulty { get; set; }
    public int nbFails { get; set; }
    public int IdChapter { get; set; }
    public string ChapterName { get; set; }
    public int IdAnswerGood { get; set; }
    public int A_id { get; set; }
    public string A_content { get; set; }
    public int total_questions { get; set;}
}
