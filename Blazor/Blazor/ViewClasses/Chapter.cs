﻿namespace Blazor.ViewClasses;

public class Chapter
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int total_chapters { get; set; }
}
