﻿namespace Blazor.ViewClasses;

public class Answer
{
    public int Id { get; set; }
    public string? Content { get; set; }
    public int IdQuestion { get; set; }

    public Answer(int id, string content, int idQuestion)
    {
        Id = id;
        Content = content;
        IdQuestion = idQuestion;
    }
}
