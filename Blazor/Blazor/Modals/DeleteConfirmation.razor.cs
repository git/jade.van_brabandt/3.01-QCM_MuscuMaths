﻿using Blazor.Services;
using Blazored.Modal.Services;
using Blazored.Modal;
using Microsoft.AspNetCore.Components;
using Blazor.ViewClasses;
using Microsoft.Extensions.Localization;

namespace Blazor.Modals
{
    public partial class DeleteConfirmation
    {
        [CascadingParameter]
        public required BlazoredModalInstance ModalInstance { get; set; }

        [Inject]
        public IStringLocalizer<DeleteConfirmation> Localizer { get; set; }

        [Inject]
        public required IDataService DataService { get; set; }

        [Parameter]
        public int Id { get; set; }

        void ConfirmDelete()
        {
            ModalInstance.CloseAsync(ModalResult.Ok(true));
        }

        void Cancel()
        {
            ModalInstance.CancelAsync();
        }
    }
}
