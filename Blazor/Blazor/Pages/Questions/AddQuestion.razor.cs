﻿using Microsoft.AspNetCore.Components;
using Blazor.Models;
using Blazor.Services;
using Blazor.Pages.Admins;
using Blazor.ViewClasses;
using static System.Net.WebRequestMethods;
using Microsoft.Extensions.Localization;

namespace Blazor.Pages.Questions
{
    public partial class AddQuestion
    {
        private QuestionModel questionModel = new();

        public List<Chapter> chapters = new();

        private List<AnswerModel> answerModels = new();
        
        public List<Checkbox> checkboxs = new();

        [Inject]
        public IStringLocalizer<AddQuestion> Localizer { get; set; }

        [Inject]
        public required IDataService DataService { get; set; }

        [Inject]
        public required NavigationManager NavigationManager { get; set; }

        [Inject]
        public required HttpClient Http { get; set; }

        [Inject]
        public required ILogger<AddAdministrator> Logger { get; set; }
        public class Checkbox
        {
            public int Id { get; set; }
            public bool IsCorrect { get; set; }

            public Checkbox(int id) { Id = id; IsCorrect = false; }
            public Checkbox() { }
            public Checkbox(int id, bool isCorrect) { Id = id; IsCorrect = isCorrect; }
        }

        public int IdAgood { get; set; }

        protected override async Task OnInitializedAsync()
        {
            var response = Http.GetFromJsonAsync<Chapter[]>(API.API_URL + "all/chapters/" + API.TOKEN + "/1").Result;
            if (response == null) chapters = new List<Chapter>();
            else chapters = new List<Chapter>(response);

            answerModels = new();
            checkboxs = new();
            for(int i = 0; i < 4; i++) 
            {
                answerModels.Add(new AnswerModel(i));
                checkboxs.Add(new Checkbox(i));
            }
        }

        private void SetCorrectAnswer(int checkboxId)
        {
            questionModel.IdAnswerGood = checkboxId;
        }
        private async Task HandleValidSubmit()
        {
            if (questionModel != null)
            {
                var formData = new List<KeyValuePair<string, string>>();
                formData.Add(new KeyValuePair<string, string>("content", questionModel.Content));
                formData.Add(new KeyValuePair<string, string>("idchapter", questionModel.IdChapter.ToString()));
                foreach (var answerModel in answerModels)
                {
                    var answercontent = $"answerContent{answerModel.Id + 1}";
                    formData.Add(new KeyValuePair<string, string>(answercontent, answerModel.Content));
                }
                var idGood = questionModel.IdAnswerGood + 1;
                formData.Add(new KeyValuePair<string, string>("idanswergood", idGood.ToString()));
                
             
                var formContent = new FormUrlEncodedContent(formData);

                string apiUri = API.API_URL + "add/question/" + API.TOKEN;

                using (var httpClient = new HttpClient())
                {
                    var response = await httpClient.PostAsync(apiUri, formContent);

                    if (response.IsSuccessStatusCode)
                    {
                        var responseBody = await response.Content.ReadAsStringAsync();
                    }
                    else
                    {
                        var errorResponse = await response.Content.ReadAsStringAsync();
                    }
                }
                
                Logger.LogInformation("Question '{questionContent}' added for Chapter '{chapterId}' with 4 answers : '{IdAnswer1}':'{AnswerContent1}', '{IdAnswer2}':'{AnswerContent2}' , '{IdAnswer3}':'{AnswerContent3}' , '{IdAnswer4}':'{AnswerContent4}' and the correct '{IdAnswerGood}' ", questionModel.Content, questionModel.IdChapter, answerModels[0].Id+1, answerModels[0].Content, answerModels[1].Id+1,answerModels[1].Content, answerModels[2].Id + 1, answerModels[2].Content, answerModels[3].Id + 1, answerModels[3].Content, IdAgood);

                NavigationManager.NavigateTo("questions");
            }
        }
    }
}
