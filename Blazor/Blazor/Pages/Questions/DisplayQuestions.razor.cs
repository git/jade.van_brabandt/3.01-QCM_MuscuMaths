﻿using Blazor.ViewClasses;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;

namespace Blazor.Pages.Questions
{
    public partial class DisplayQuestions
    {
        [Parameter]
        public int QuestionId { get; set; }

        public Question question = new();

        private List<Answer> answers = new();
        public List<Question> questions = new();
        [Inject]
        public IStringLocalizer<DisplayQuestions> Localizer { get; set; }
        [Inject]
        public required HttpClient Http { get; set; }

        protected override async Task OnInitializedAsync()
        {

            var response = Http.GetFromJsonAsync<Question[]>(API.API_URL + "questions/" + QuestionId + "/" + API.TOKEN).Result;
            questions = new List<Question>(response);

            question = questions.Find(q => q.Id == QuestionId);

            IEnumerable<Question> foundQuestions = questions.Where(q => q.Id == QuestionId);


            foreach (var q in foundQuestions)
            {
                answers.Add(new Answer(q.A_id, q.A_content, q.Id));

            }
        }

    }
}
