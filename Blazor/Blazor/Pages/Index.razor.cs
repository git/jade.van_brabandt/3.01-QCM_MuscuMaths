﻿using Blazor.Pages.Questions;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;

namespace Blazor.Pages
{
    public partial class Index
    {
        [Inject]
        public IStringLocalizer<Index> Localizer { get; set; }

        [Inject]
        public IHostApplicationLifetime  AppLifetime { get; set; }

        [Inject]
        public NavigationManager NavigationManager { get; set; }

        private void CloseApplication()
        {
            NavigationManager.NavigateTo("http://localhost:8888", forceLoad: true);
            AppLifetime.StopApplication();
        }
    }
}
