﻿using System.ComponentModel.DataAnnotations;

namespace Blazor.Models
{
    public class ChapterModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [StringLength(50, ErrorMessage = "Name is too long.")]
        public string Name { get; set; }
    }
}
