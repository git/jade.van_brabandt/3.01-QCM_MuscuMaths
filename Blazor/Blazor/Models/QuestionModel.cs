﻿namespace Blazor.Models;
using System.ComponentModel.DataAnnotations;

public class QuestionModel
{
    public int Id { get; set; }

    [Required(ErrorMessage = "Content question is required")]
    [StringLength(255, ErrorMessage = "Content question is too long.")]
    public string Content { get; set; }

    [Required(ErrorMessage = "Chapter is required")]
    public int IdChapter { get; set; }
    public int IdAnswerGood { get; set; }
    public int Difficulty { get; set; }
    public int NbFails { get; set; }

    public void addFails(int nb) { NbFails += nb; }
    public void removeFails(int nb) { NbFails -= nb; }
}
