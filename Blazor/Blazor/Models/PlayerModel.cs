﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System.ComponentModel.DataAnnotations;
using System.Security.Cryptography;
using System.Text;

namespace Blazor.Models;

public class PlayerModel
{
    public int Id { get; set; }

    [Required(ErrorMessage = "Nickname is required")]
    [RegularExpression(@"^[\p{L}0-9]+$", ErrorMessage = "La chaîne doit être composée uniquement de lettres, des chiffres et peut inclure des caractères accentués, sans espaces.")]
    [StringLength(20, ErrorMessage = "{0} length must be between {2} and {1}.", MinimumLength = 3)]
    public string Nickname { get; set; }

    [Required(ErrorMessage = "Password is required")]
    [RegularExpression(@"^[^\s]+$", ErrorMessage = "Le champ ne doit pas contenir d'espaces.")]
    [StringLength(255, ErrorMessage = "{0} length must be between {2} and {1}.", MinimumLength = 6)]
    public string HashedPassword { get; set; }

    public void HashPassword(string password)
    {
        this.HashedPassword = BCrypt.Net.BCrypt.HashPassword(password, BCrypt.Net.BCrypt.GenerateSalt());
    }

    //public void HashPassword(string password)
    //{
    //    using (MD5 md5 = MD5.Create())
    //    {
    //        byte[] inputBytes = Encoding.UTF8.GetBytes(password);
    //        byte[] hashBytes = md5.ComputeHash(inputBytes);

    //        StringBuilder sb = new StringBuilder();
    //        for (int i = 0; i < hashBytes.Length; i++)
    //        {
    //            sb.Append(hashBytes[i].ToString("x2"));
    //        }

    //        HashedPassword = sb.ToString();
    //    }
    //}
}
