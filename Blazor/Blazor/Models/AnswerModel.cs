﻿using System.ComponentModel.DataAnnotations;

namespace Blazor.Models;

public class AnswerModel

{
    public int Id { get; set; }

    [Required(ErrorMessage = "Content answer is required")]
    [StringLength(40, ErrorMessage = "Content answer is too long.")]
    public string? Content { get; set; }
    public int? IdQuestion { get; set; }
    public AnswerModel(int id)
    {
        Id = id;
    }

    public AnswerModel(){}
}
