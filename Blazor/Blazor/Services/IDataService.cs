﻿using Blazor.Models;
using Blazor.ViewClasses;

namespace Blazor.Services
{
    public interface IDataService
    {
        Task<Chapter> GetById(int id);

        Task<Administrator> GetAdminById(int id);

        Task<List<Question>> GetQuestionsById(int id);

        Task<Player> GetPlayerById(int id);

        Task<Answer> GetAnswerByIdQuestion(int id);
    }
}
