﻿using Blazor.Models;
using Blazor.Pages;
using Blazor.ViewClasses;
using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using static System.Net.WebRequestMethods;

namespace Blazor.Services
{
    public class DataLocalService : IDataService
    {
        private readonly HttpClient _http;
        private readonly ILocalStorageService _localStorage;
        private readonly NavigationManager _navigationManager;

        public DataLocalService(
            ILocalStorageService localStorage,
            HttpClient http,
            NavigationManager navigationManager)
        {
            _localStorage = localStorage;
            _http = http;
            _navigationManager = navigationManager;
        }

        public async Task<Chapter> GetById(int id)
        {
            // Get the current data
            var currentData = _http.GetFromJsonAsync<List<Chapter>>(API.API_URL + "chapters/" + id + "/" + API.TOKEN).Result;
            var chapter = currentData[0];

            // Check if chapter exist
            if (chapter == null)
            {
                throw new Exception($"Unable to found the item with ID: {id}");
            }

            return chapter;
        }

        public async Task<Administrator> GetAdminById(int id)
        {
            // Get the current data
            var currentData = _http.GetFromJsonAsync<List<Administrator>>(API.API_URL + "administrators/" + id + "/" + API.TOKEN).Result;
            var admin = currentData[0];

            // Check if admin exist
            if (admin == null)
            {
                throw new Exception($"Unable to found the item with ID: {id}");
            }

            return admin;
        }

        public async Task<Question> GetQuestionById(int id)
        {
            // Get the current data
            var currentData = _http.GetFromJsonAsync<List<Question>>(API.API_URL + "questions/"+API.TOKEN).Result;

            // Get the question int the list
            var question = currentData.FirstOrDefault(w => w.Id == id);

            // Check if question exist
            if (question == null)
            {
                throw new Exception($"Unable to found the item with ID: {id}");
            }

            return question;
        }

        public async Task<List<Question>> GetQuestionsById(int id)
        {
            // Get the current data
            var currentData = await _http.GetFromJsonAsync<List<Question>>(API.API_URL + "questions/" + API.TOKEN);
            
            // Get all questions with the specified ID
            var questions = currentData.Where(w => w.Id == id).ToList();

            // Check if any questions were found
            if (questions.Count == 0)
            {
                throw new Exception($"Unable to find questions with ID: {id}");
            }

            return questions;
        }

        public async Task<Player> GetPlayerById(int id)
        {
            // Get the current data
            var currentData = _http.GetFromJsonAsync<List<Player>>(API.API_URL + "players/" + id + "/" + API.TOKEN).Result;
            var player = currentData[0];

            // Check if player exist
            if (player == null)
            {
                throw new Exception($"Unable to found the item with ID: {id}");
            }

            return player;
        }

        public async Task<Answer> GetAnswerByIdQuestion(int id)
        {
            // Get the current data
            var answer = _http.GetFromJsonAsync<Answer>(API.API_URL + "answer/" + id + "/" + API.TOKEN).Result;

            // Check if question exist
            if (answer == null)
            {
                throw new Exception($"Unable to found the item with ID: {id}");
            }

            return answer;
        }
    }

}