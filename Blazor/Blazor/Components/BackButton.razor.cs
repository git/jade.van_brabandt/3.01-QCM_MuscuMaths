﻿using Blazor.Pages.Questions;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;

namespace Blazor.Components
{
    public partial class BackButton
    {

        [Parameter]
        public string RedirectionPage { get; set; }

        [Inject]
        public IStringLocalizer<BackButton> Localizer { get; set; }

        [Inject]
        public required NavigationManager NavigationManager { get; set; }
        private void Back()
        {
            NavigationManager.NavigateTo(RedirectionPage, true);
        }
    }
}
