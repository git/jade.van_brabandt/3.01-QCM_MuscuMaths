﻿using Blazor.Pages;
using Blazor.ViewClasses;
using Microsoft.AspNetCore.Components;

namespace Blazor.Components
{
    public partial class CardViewQuestion
    {
        [Parameter]
        public Question Question { get; set; }

        [Parameter]
        public List<Answer> Answers { get; set; }
    }
}
