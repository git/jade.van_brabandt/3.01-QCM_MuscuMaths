﻿using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;

namespace Blazor.Shared
{
    public partial class CultureSelector
    {
        [Inject]
        public IStringLocalizer<CultureSelector> Localizer { get; set; }
    }
}
