<?php

namespace gateways;

use usages\Connection;
use \PDO;

class GatewayAdministrator
{
    private $con;

    public function __construct()
    {
        global $dns, $user, $pass;
        if ($dns == NULL || $user == NULL || $pass == NULL) {
            require_once(__DIR__ . '/../usages/Config_DB.php');
        }
        $this->con = new Connection($dns, $user, $pass);
    }

    public function addAdministrator($administrator)
    {
        $query = "insert into administrators(username,password) values (:username,:password);";
        $this->con->executeQuery(
            $query,
            array(
                ':username' => array($administrator['username'], PDO::PARAM_STR),
                ':password' => array(md5($administrator['password']), PDO::PARAM_STR)
            )
        );
    }

    public function getAdministratorByUsername(string $username)
    {
        $query = "SELECT * FROM administrators WHERE username = :username;";
        $this->con->executeQuery($query, array(':username' => array($username, PDO::PARAM_STR)));
        $results = $this->con->getResults();
        if ($results == NULL) {
            return false;
        }
        return $results[0];
    }

    public function getAdministratorByID(int $id)
    {
        $query = "SELECT * FROM administrators WHERE id = :id;";
        $this->con->executeQuery($query, array(':id' => array($id, PDO::PARAM_INT)));
        $results = $this->con->getResults();
        if ($results == NULL) {
            return ["id" => null];
        }
        return $results[0];
    }

    public function getAdministrators()
    {
        $query = "SELECT * FROM administrators";
        $this->con->executeQuery($query);
        $results = $this->con->getResults();

        return $results;
    }

    public function updateAdministrator($id, $administrator)
    {
        $query = "UPDATE administrators SET username = :username, password = :password WHERE id = :id;";
        $this->con->executeQuery(
            $query,
            array(
                ':id' => array($id, PDO::PARAM_INT),
                ':username' => array($administrator['username'], PDO::PARAM_STR),
                ':password' => array(md5($administrator['password']), PDO::PARAM_STR)
            )
        );
    }

    public function deleteAdministratorByID($id)
    {
        $query = "DELETE FROM administrators WHERE id = :id;";
        $this->con->executeQuery(
            $query,
            array(
                ':id' => array($id, PDO::PARAM_INT)
            )
        );
    }

    public function verifyAdministrator($administrator)
    {
        $query = "SELECT administrators.id,administrators.password FROM administrators WHERE username = :username";
        $this->con->executeQuery(
            $query,
            array(
                ':username' => array($administrator['username'], PDO::PARAM_STR),
                //':password' => array((password_hash($administrator['password'], PASSWORD_BCRYPT)), PDO::PARAM_STR)
            )
        );
        $results = $this->con->getResults();
        if (password_verify($administrator['password'], $results[0]['password'])) {
            return $results[0];
        }
        return $results[0];
    }
    public function verifyAdministratorByName($administrator)
    {
        $query = "SELECT administrators.id FROM administrators WHERE username = :username";
        $this->con->executeQuery(
            $query,
            array(
                ':username' => array($administrator['username'], PDO::PARAM_STR),
            )
        );
        $results = $this->con->getResults();

        return $results[0];
    }
}
