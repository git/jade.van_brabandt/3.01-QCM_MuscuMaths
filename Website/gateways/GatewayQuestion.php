<?php

namespace gateways;

use usages\Connection;
use \PDO;

class GatewayQuestion
{
    private $con;

    public function __construct()
    {
        global $dns, $user, $pass;
        if ($dns == NULL || $user == NULL || $pass == NULL) {
            require_once(__DIR__ . '/../usages/Config_DB.php');
        }
        $this->con = new Connection($dns, $user, $pass);
    }

    public function addQuestion($question)
    {
        $query = "insert into questions(content,idchapter,difficulty,nbfails) values (:content,:idchapter,:difficulty,:nbfails);";
        $this->con->executeQuery(
            $query,
            array(
                ':content' => array($question['content'], PDO::PARAM_STR),
                ':idchapter' => array($question['idchapter'], PDO::PARAM_INT),
                ':difficulty' => array(intval($question['difficulty']), PDO::PARAM_INT),
                ':nbfails' => array(intval($question['nbfails']), PDO::PARAM_INT)
            )
        );
        $questionId = $this->con->lastInsertId();
        return $questionId;
    }

    public function getQuestionByID($id)
    {
        $query = "SELECT * FROM questions WHERE id = :id;";
        $this->con->executeQuery($query, array(':id' => array($id, PDO::PARAM_INT)));
        $results = $this->con->getResults();
        return $results[0];
    }

    public function getQuestions()
    {
        $query = "SELECT * FROM questions";
        $this->con->executeQuery($query);
        $results = $this->con->getResults();
        return $results;
    }

    public function updateQuestion($id, $questionDataArray)
    {
        $query = "UPDATE questions SET content = :content, idchapter = :idchapter, idanswergood = :idanswergood WHERE id = :id;";
        $this->con->executeQuery(
            $query,
            array(
                ':content' => array($questionDataArray['content'], PDO::PARAM_STR),
                ':idchapter' => array($questionDataArray['idchapter'], PDO::PARAM_INT),
                ':idanswergood' => array($questionDataArray['idanswergood'], PDO::PARAM_INT),
                ':id' => array($id, PDO::PARAM_INT),
            )
        );
    }

    public function updateNbFails($question)
    {
        $query = "UPDATE questions SET nbfails = :nbfails WHERE id = :id;";
        $this->con->executeQuery(
            $query,
            array(
                ':nbfails' => array($question->getNbFails(), PDO::PARAM_INT),
                ':id' => array($question->getId(), PDO::PARAM_INT),
            )
        );
    }

    public function updateDifficulty($question)
    {
        $query = "UPDATE questions SET difficulty = :difficulty WHERE id = :id;";
        $this->con->executeQuery(
            $query,
            array(
                ':difficulty' => array($question->getDifficulty(), PDO::PARAM_INT),
                ':id' => array($question->getId(), PDO::PARAM_INT),
            )
        );
    }

    public function deleteQuestionByID($id)
    {
        $query = "DELETE FROM questions WHERE id = :id;";
        $this->con->executeQuery(
            $query,
            array(
                ':id' => array($id, PDO::PARAM_INT)
            )
        );
    }

    public function getQuestionsByChapterAndDifficulty($idChapter, $difficulty)
    {
        $query = "SELECT * FROM questions WHERE idchapter = :idChapter AND difficulty = :difficulty ORDER BY RAND() LIMIT 10;";
        $this->con->executeQuery(
            $query,
            array(
                ':idChapter' => array($idChapter, PDO::PARAM_INT),
                ':difficulty' => array($difficulty, PDO::PARAM_INT),
            )
        );
        $results = $this->con->getResults();

        return $results;
    }
}
