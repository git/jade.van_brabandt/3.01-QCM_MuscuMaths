<?php

namespace gateways;

use usages\Connection;
use \PDO;

class GatewayLobby
{
    private $con;

    public function __construct()
    {
        global $dns, $user, $pass;
        if ($dns == NULL || $user == NULL || $pass == NULL) {
            require_once(__DIR__ . '/../usages/Config_DB.php');
        }
        $this->con = new Connection($dns, $user, $pass);
    }
    public function addLobby($lobby)
    {
        $query = "insert into lobbies(id,name,password,nbplayers) values (:id,:name,:password,:nbplayers);";
        $this->con->executeQuery(
            $query,
            array(
                ':id' => array($lobby->getId(), PDO::PARAM_INT),
                ':name' => array($lobby->getName(), PDO::PARAM_STR),
                ':password' => array($lobby->getPassword(), PDO::PARAM_STR),
                ':nbplayers' => array($lobby->getNbPlayers(), PDO::PARAM_INT)
            )
        );
    }

    public function getLobbies()
    {
        $query = "SELECT * FROM lobbies;";
        $this->con->executeQuery($query);
        $results = $this->con->getResults();

        return $results;
    }

    public function getLobbyByName($name)
    {
        $query = "SELECT * FROM lobbies WHERE name = :name;";
        $this->con->executeQuery($query, array(':name' => array($name, PDO::PARAM_STR)));
        $results = $this->con->getResults();
        if ($results == NULL) {
            return false;
        }
        return $results[0];
    }


    public function deleteLobby($id)
    {
        $query = "DELETE FROM lobbies WHERE id = :id;";
        $this->con->executeQuery($query, array(':id' => array($id, PDO::PARAM_INT)));
    }


}
