<?php

namespace gateways;

use usages\Connection;
use \PDO;

class GatewayJouer
{
    private $con;

    public function __construct()
    {
        global $dns, $user, $pass;
        if ($dns == NULL || $user == NULL || $pass == NULL) {
            require_once(__DIR__ . '/../usages/Config_DB.php');
        }
        $this->con = new Connection($dns, $user, $pass);
    }

    public function addJouer($jouer)
    {
        $query = "INSERT into jouer(idchapter,idplayer,maxscore) values (:idchapter,:idplayer,:maxscore);";
        $this->con->executeQuery(
            $query,
            array(
                'idchapter' => array($jouer['idchapter'], PDO::PARAM_INT),
                'idplayer' => array($jouer['idplayer'], PDO::PARAM_INT),
                'maxscore' => array($jouer['maxscore'], PDO::PARAM_INT)
            )
        );
    }

    public function getMaxScoreByPlayerAndChapter($jouer)
    {
        $query = "SELECT jouer.maxscore FROM jouer WHERE jouer.idplayer = :idplayer AND jouer.idchapter = :idchapter";
        $this->con->executeQuery(
            $query,
            array(
                ':idchapter' => array($jouer['idchapter'], PDO::PARAM_INT),
                ':idplayer' => array($jouer['idplayer'], PDO::PARAM_INT)
            )

        );
        $results = $this->con->getResults();
        return $results[0];
    }

    public function updateJouer($jouer)
    {
        $query = "UPDATE jouer SET jouer.maxscore = :maxscore WHERE jouer.idplayer = :idplayer AND jouer.idchapter = :idchapter AND jouer.maxscore <= :maxscore;";
        $this->con->executeQuery(
            $query,
            array(
                ':idchapter' => array($jouer['idchapter'], PDO::PARAM_INT),
                ':idplayer' => array($jouer['idplayer'], PDO::PARAM_INT),
                ':maxscore' => array($jouer['maxscore'], PDO::PARAM_INT)
            )
        );
    }

    public function verifyJouer($jouer)
    {
        $query = "SELECT jouer.idchapter, jouer.idplayer FROM jouer WHERE jouer.idplayer = :idplayer AND jouer.idchapter = :idchapter";
        $this->con->executeQuery(
            $query,
            array(
                ':idchapter' => array($jouer['idchapter'], PDO::PARAM_INT),
                ':idplayer' => array($jouer['idplayer'], PDO::PARAM_INT)
            )
        );
        $results = $this->con->getResults();
        if (count($results) == 0) {
            return false;
        }
        return $results[0];
    }
    public function getMaxScoresWithChapter($player)
    {
        $query = "SELECT maxscore,idchapter FROM jouer WHERE idplayer = :idplayer;";
        $this->con->executeQuery(
            $query,
            array(
                ':idplayer' => array($player->getId(), PDO::PARAM_STR)
            )
        );
        $results = $this->con->getResults();
        return $results;
    }
    public function deleteJouer($jouer)
    {
        $query = "DELETE FROM jouer WHERE jouer.idplayer = :idplayer AND jouer.idchapter = :idchapter;";
        $this->con->executeQuery(
            $query,
            array(
                ':idchapter' => array($jouer['idchapter'], PDO::PARAM_INT),
                ':idplayer' => array($jouer['idplayer'], PDO::PARAM_INT)
            )
        );
    }
}