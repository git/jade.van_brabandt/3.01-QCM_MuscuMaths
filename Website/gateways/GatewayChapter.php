<?php

namespace gateways;

use usages\Connection;
use \PDO;

class GatewayChapter
{
    private $con;

    public function __construct()
    {
        global $dns, $user, $pass;
        if ($dns == null || $user == null || $pass == null) {
            require_once(__DIR__ . '/../usages/Config_DB.php');
        }
        $this->con = new Connection($dns, $user, $pass);
    }

    public function addChapter($chapter)
    {
        $query = "insert into chapters(name) values (:name);";
        $this->con->executeQuery(
            $query,
            array(
                ':name' => array($chapter['name'], PDO::PARAM_STR)
            )
        );
        if ($this->con->lastInsertId() == null) {
            return null;
        }
        return $this->con->lastInsertId();
    }

    public function getChapters()
    {
        $query = "SELECT * FROM chapters";
        $this->con->executeQuery($query);
        $results = $this->con->getResults();

        return $results;
    }

    public function getChapterByID(int $id)
    {
        $query = "SELECT * FROM chapters WHERE id = :id;";
        $this->con->executeQuery($query, array(':id' => array($id, PDO::PARAM_INT)));
        $results = $this->con->getResults();

        if (count($results) == 0) {
            return null;
        }
        return $results[0];
    }

    public function updateChapter($id, $chapter)
    {
        $query = "UPDATE chapters SET name = :name WHERE id = :id;";
        $this->con->executeQuery(
            $query,
            array(
                ':id' => array($id, PDO::PARAM_INT),
                ':name' => array($chapter['name'], PDO::PARAM_STR)
            )
        );
    }

    public function deleteChapter($id)
    {
        $query = "DELETE FROM chapters WHERE id = :id;";
        $this->con->executeQuery(
            $query,
            array(
                ':id' => array($id, PDO::PARAM_INT)
            )
        );
        return $this->con->getResults();
    }

    public function verifyChapterByID($id)
    {
        $query = "SELECT chapters.id FROM chapters WHERE id = :id;";
        $this->con->executeQuery(
            $query,
            array(
                ':id' => array($id, PDO::PARAM_STR),
            )
        );
        $results = $this->con->getResults();
        if (count($results) == 0) {
            return null;
        }
        return $results[0];
    }
    public function verifyChapterByName($name)
    {
        $query = "SELECT * FROM chapters WHERE name = :name;";
        $this->con->executeQuery(
            $query,
            array(
                ':name' => array($name, PDO::PARAM_STR)
            )
        );
        $results = $this->con->getResults();
        if (count($results) == 0) {
            return null;
        }

        return $results[0];
    }
}
