<?php

namespace models;

use gateways\GatewayAnswer;
use classes\Answer;

class ModelAnswer
{
    private $gwAnswer;

    public function __construct()
    {
        $this->gwAnswer = new GatewayAnswer();
    }

    function addAnswer($answer)
    {
        $answersId = $this->gwAnswer->addAnswer($answer);
        return $answersId;
    }

    function getAnswerByID($id)
    {
        $answerDataArray = $this->gwAnswer->getAnswerByID($id);
        $answer = new Answer($id, $answerDataArray['content'], $answerDataArray['idquestion']);
        return $answer;
    }

    function getAnswersByIDQuestions($idQuestion)
    {
        $answersDataArray = $this->gwAnswer->getAnswersByIDQuestions($idQuestion);
        if ($answersDataArray == null) {
            return null;
        } else {
            $answers = array();
            foreach ($answersDataArray as $answerDataArray) {
                $answer = new Answer($answerDataArray['id'], $answerDataArray['content'], $idQuestion);
                $answers[] = $answer;
            }
            return $answers;
        }
    }

    function updateAnswer($answersId, $answer)
    {
        $this->gwAnswer->updateAnswer($answersId, $answer);
    }
}
