<?php

namespace models;

use gateways\GatewayLobby;
use classes\Lobby;

class ModelLobby
{
    private $gwLobby;

    public function __construct()
    {
        $this->gwLobby = new GatewayLobby();
    }

    public function addLobby($lobby)
    {
        $this->gwLobby->addLobby($lobby);
    }

    public function getLobbies()
    {
        $lobbiesDataArray = $this->gwLobby->getLobbies();
        $lobbies = array();
        
        foreach ($lobbiesDataArray as $lobbyDataArray) {
            $lobby = new Lobby(
                intval($lobbyDataArray['id']),
                $lobbyDataArray['name'],
                $lobbyDataArray['password'],
                intval($lobbyDataArray['nbplayers'])
            );
            $lobbies[] = $lobby;
        }
        
        return $lobbies; // Move the return statement outside the foreach loop
    }
}