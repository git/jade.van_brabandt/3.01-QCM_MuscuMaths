<?php

namespace models;

use gateways\GatewayQuestion;
use classes\Question;

class ModelQuestion
{
    private $gwQuestion;
    public $questions;

    public function __construct()
    {
        $this->gwQuestion = new GatewayQuestion();
    }

    function getQuestions(): array
    {
        $questionsDataArray = $this->gwQuestion->getQuestions();
        $questions = array();
        foreach ($questionsDataArray as $questionDataArray) {
            $question = new Question(
                intval($questionDataArray['id']),
                $questionDataArray['content'],
                intval($questionDataArray['idchapter']),
                intval($questionDataArray['idanswergood']),
                intval($questionDataArray['difficulty']),
                intval($questionDataArray['nbfails'])
            );
            $questions[] = $question;
        }
        return $questions;
    }




    function deleteQuestionByID($id)
    {
        $this->gwQuestion->deleteQuestionByID($id);
    }

    function addQuestion($questionsDataArray)
    {
        $questionId = $this->gwQuestion->addQuestion($questionsDataArray);
        return $questionId;
    }

    function getQuestionByID($id)
    {
        $questionDataArray = $this->gwQuestion->getQuestionByID($id);
        if ($questionDataArray == null) {
            return null;
        } else {
            $question = new Question(
                intval($questionDataArray['id']),
                $questionDataArray['content'],
                intval($questionDataArray['idchapter']),
                intval($questionDataArray['idanswergood']),
                intval($questionDataArray['difficulty']),
                intval($questionDataArray['nbfails'])
            );
            return $question;
        }
    }

    function updateQuestion($id, $questionDataArray)
    {
        $this->gwQuestion->updateQuestion($id, $questionDataArray);
    }

    function updateNbFails($question){
        $this->gwQuestion->updateNbFails($question);
    }

    function updateDifficulty($question){
        $this->gwQuestion->updateDifficulty($question);
    }

    function getQuestionsByChapterAndDifficulty($chapter, $difficulty)
    {
        $questionsDataArray = $this->gwQuestion->getQuestionsByChapterAndDifficulty($chapter, $difficulty);
        $this->questions = array();
        foreach ($questionsDataArray as $questionDataArray) {
            $question = new Question(
                intval($questionDataArray['id']),
                $questionDataArray['content'],
                intval($questionDataArray['idchapter']),
                intval($questionDataArray['idanswergood']),
                intval($questionDataArray['difficulty']),
                intval($questionDataArray['nbfails'])
            );
            $this->questions[] = $question;
        }
        return $this->questions;
    }
}
