<?php

namespace models;

use gateways\GatewayPlayer;
use gateways\GatewayJouer;
use classes\Player;

class ModelPlayer
{
    private $gwPlayer;
    private $gwJouer;

    public function __construct()
    {
        $this->gwPlayer = new GatewayPlayer();
        $this->gwJouer = new GatewayJouer();
    }

    public function addPlayer($player)
    {
        var_dump($player);
        $this->gwPlayer->addPlayer($player);
    }

    public function verifyPlayer($player)
    {
        $playerId = $this->gwPlayer->verifyPlayer($player);
        return $playerId;
    }

    public function getPlayerByID($id)
    {
        $playerDataArray = $this->gwPlayer->getPlayerByID($id);
        $player = new Player($playerDataArray["id"], $playerDataArray["nickname"], $playerDataArray["password"]);
        return $player;
    }

    public function updatePlayer($id, $player)
    {
        $this->gwPlayer->updatePlayer($id, $player);
    }
    public function updatePlayerPassword($id, $player)
    {
        $this->gwPlayer->updatePlayerPassword($id, $player);
    }
    public function deletePlayerByID($id)
    {
        $this->gwPlayer->deletePlayerByID($id);
    }

    public function addJouer($jouer)
    {
        $this->gwJouer->addJouer($jouer);
    }

    public function getMaxScoreByPlayerAndChapter($jouer)
    {
        $jouerDataArray = $this->gwJouer->getMaxScoreByPlayerAndChapter($jouer);
        return $jouerDataArray;
    }

    public function updateJouer($jouer)
    {
        $this->gwJouer->updateJouer($jouer);
    }

    public function getMaxScoresWithChapter($player)
    {
        $maxScores = $this->gwJouer->getMaxScoresWithChapter($player);
        return $maxScores;
    }

    public function verifyJouer($jouer)
    {
        $tabidjouer = $this->gwJouer->verifyJouer($jouer);
        return $tabidjouer;
    }

}
