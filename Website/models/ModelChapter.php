<?php

namespace models;

use gateways\GatewayChapter;
use classes\Chapter;

class ModelChapter
{
    private $gwChapter;

    public function __construct()
    {
        $this->gwChapter = new GatewayChapter();
    }

    function getChapters()
    {
        $chaptersDataArray = $this->gwChapter->getChapters();
        if ($chaptersDataArray == null) {
            return null;
        } else {
            $chapters = array();
            foreach ($chaptersDataArray as $chapterDataArray) {
                $chapter = new Chapter($chapterDataArray['id'], $chapterDataArray["name"]);
                $chapters[] = $chapter;
            }
            return $chapters;
        }
    }

    function deleteChapter($id)
    {
        $this->gwChapter->deleteChapter($id);
    }

    function addChapter($chapter)
    {
        $this->gwChapter->addChapter($chapter);
    }

    function getChapterByID($id)
    {
        $chapterDataArray = $this->gwChapter->getChapterByID($id);
        if ($chapterDataArray == null) {
            return null;
        } else {
            $chapter = new Chapter($chapterDataArray['id'], $chapterDataArray['name']);
            return $chapter;
        }
    }

    function updateChapter($id, $chapter)
    {
        $this->gwChapter->updateChapter($id, $chapter);
    }
    public function verifyChapter($chapter)
    {
        $id = $this->gwChapter->verifyChapterByID($chapter);
        return $id;
    }
    public function verifyChapterByName($name)
    {
        $id = $this->gwChapter->verifyChapterByName($name);
        if ($id == null) {
            return null;
        }
        return $id;
    }
}
