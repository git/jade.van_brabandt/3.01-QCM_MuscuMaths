<?php

namespace models;

use gateways\GatewayAdministrator;
use classes\Administrator;

class ModelAdministrator
{
    private $gwAdministrator;

    public function __construct()
    {
        $this->gwAdministrator = new GatewayAdministrator();
    }

    public function addAdministrator($administrator)
    {
        $this->gwAdministrator->addAdministrator($administrator);
    }

    public function getAdministratorByID($id)
    {
        $administratorDataArray = $this->gwAdministrator->getAdministratorByID($id);
        if ($administratorDataArray == null) {
            return null;
        } else {
            $administrator = new Administrator($administratorDataArray["id"], $administratorDataArray["username"], $administratorDataArray["password"]);
            return $administrator;
        }
    }

    public function getAdministrators()
    {
        $administratorsDataArray = $this->gwAdministrator->getAdministrators();
        $administrators = array();
        foreach ($administratorsDataArray as $administratorDataArray) {
            $administrator = new Administrator($administratorDataArray["id"], $administratorDataArray["username"], $administratorDataArray["password"]);
            $administrators[] = $administrator;
        }
        return $administrators;
    }

    public function updateAdministrator($id, $administrator)
    {
        $this->gwAdministrator->updateAdministrator($id, $administrator);
    }

    public function deleteAdministratorByID($id)
    {
        $this->gwAdministrator->deleteAdministratorByID($id);
    }

    public function verifyAdministrator($Administrator)
    {
        $administratorsId = $this->gwAdministrator->verifyAdministrator($Administrator);
        return $administratorsId;
    }
    public function verifyAdministratorByName($Administrator)
    {
        $administratorsId = $this->gwAdministrator->verifyAdministratorByName($Administrator);
        return $administratorsId;
    }
}
