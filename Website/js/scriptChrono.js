import { passerAction } from "./passer.js";
let tempsExport = 0;
const dureeQuiz = 30; // Durée du quiz en secondes
const aiguilleElement = document.getElementById("aiguille");
const fondElement = document.getElementById("fond");
let tempsRestant = dureeQuiz;
let debutAnimation;
const animationDuration = 30 * 1000; // Durée de l'animation en millisecondes


function mettreAJourTempsExport(newTemps) {
    tempsExport = newTemps;
}

function mettreAJourAiguille(timestamp) {
    if (!debutAnimation) {
        debutAnimation = timestamp;
    }
    const tempsEcoule = timestamp - debutAnimation;
    mettreAJourTempsExport(tempsEcoule);
    const pourcentageTempsEcoule = tempsEcoule / animationDuration;
    const rotationDeg = pourcentageTempsEcoule * 360;
    aiguilleElement.style.transform = `rotate(${rotationDeg}deg)`;
    // Mettez à jour le fond en fonction de la position de l'aiguille
    fondElement.style.background = `conic-gradient(red 0%, red ${rotationDeg}deg, #4b4b4b ${rotationDeg}deg, #4b4b4b 360deg)`;

    if (tempsEcoule < animationDuration) {
        requestAnimationFrame(mettreAJourAiguille);
    } else {
        passerAction();
    }
}
requestAnimationFrame(mettreAJourAiguille);
export { tempsExport };