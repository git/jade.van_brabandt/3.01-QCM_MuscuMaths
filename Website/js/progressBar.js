import { tempsExport } from "./scriptChrono.js";

$(document).ready(function() {
    var timerId, percent;
    percent = 0;
    var isPaused = false;

    // reset progress bar
    percent = 0;
    //$('#test').attr('disabled', true);
    $('#load').css('width', '0px');
    $('#load').addClass('progress-bar-striped active');
    $('#rocket').show();

    timerId = setInterval(function() {
        if(!isPaused){
            // increment progress bar
            percent += 1/12;
            $('#load').css('width', percent + '%');
            $('#load').html(percent + '%');
            $('#rocket').css('left','calc('+percent+'% - 19px');
            
            if(percent>= 100){
                clearInterval(timerId);
                $('#load').removeClass('progress-bar-striped active');
                $('#load').addClass('bg-danger');
                $('#repondbien').attr('disabled', true);
                $('#load').html('Finish');
                $('#rocket').hide();
            }
        }
    }, 250);

    function wait(ms){
        var start = new Date().getTime();
        var end = start;
        while(end < start + ms) {
            end = new Date().getTime();
        }
    }
    
    $('#repondbien').click(function() {
        if (tempsExport <= 6000) {
            percent += 12;
        } else if (tempsExport > 6000 && tempsExport <= 12000) {
            percent += 11;
        } else if (tempsExport > 12000 && tempsExport <= 18000) {
            percent += 10;
        } else if (tempsExport > 18000 && tempsExport <= 24000) {
            percent += 9;
        } else {
            percent += 8;
        }
        $('#repondbien').attr('disabled', false);
        $('#load').css('width', '0px');
        $('#load').addClass('progress-bar-striped active');
        $('#load').removeClass('bg-danger');
        $('#load').css('width', percent + '%');
        $('#load').html(percent + '%');
        
    })

    $('#repondmal').click(function() {
        isPaused = true;
        $('#repondmal').attr('disabled', false);
        //$('#load').css('width', percent+'px');
        $('#load').addClass('progress-bar-striped active');
        $('#load').removeClass('bg-danger');
        wait(3000);
        console.log("test");
        isPaused = false;
    })
})