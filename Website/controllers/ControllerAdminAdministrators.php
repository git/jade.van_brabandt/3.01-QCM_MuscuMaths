<?php

namespace controllers;

use \Exception;
use \PDOException;
use models\ModelAdministrator;

class ControllerAdminAdministrators
{
    private $mdAdministrator;

    private $twig;
    private $vues;

    function __construct()
    {
        global $vues, $twig;
        session_start();
        try {

            if ($_SESSION["idAdminConnected"] != null) {
                $this->twig = $twig;
                $this->vues = $vues;

                $this->mdAdministrator = new ModelAdministrator();

                $administrators = $this->mdAdministrator->getAdministrators();

                echo $twig->render($vues["adminAdministrators"], [
                    'administrators' => $administrators,
                    'error' => $_SESSION["error"],
                ]);
                $_SESSION["error"] = null;
            } else {
                header("Location:/loginAdmin");
            }
        } catch (PDOException $e) {
            // Gérez les erreurs PDO ici
        } catch (Exception $e2) {
            // Gérez d'autres erreurs ici
        }
    }

    function delete($param)
    {
        $this->mdAdministrator->deleteAdministratorByID($param["id"]);
        header("Location:/admin/administrators");
    }

    function add($param)
    {
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            $_SESSION["error"] = "Méthode non autorisée.";
            header("Location:/admin/administrators");
        } else {
            $username = $_POST['username'];
            $password = $_POST['password'];
            $username = trim($_POST['username']);
            $password = trim($_POST['password']);
            if (!isset($username) || !isset($password) || empty($username) || empty($password)) {
                $_SESSION["error"] = "Veuillez remplir tous les champs.";
                header("Location:/admin/administrators");
            } else {
                $Admin = [
                    'username' => $username,
                    'password' => $password,
                ];
                if ($this->mdAdministrator->verifyAdministratorByName($Admin) != null) {
                    $_SESSION["error"] = "Cet admin existe déjà.";
                    header("Location:/admin/administrators");
                } else {
                    $this->mdAdministrator->addAdministrator($Admin);
                    header("Location:/admin/administrators");
                }
            }
        }
    }

    function updatemodal($param)
    {
        if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
            $_SESSION["error"] = "Méthode non autorisée.";
            header("Location:/admin/administrators");
        } else {
            $administrator = $this->mdAdministrator->getAdministratorByID($param["id"]) ?? null;
            if ($administrator == null) {
                $_SESSION["error"] = "Cet admin n'existe pas.";
                header("Location:/admin/administrators");
            } else {
                echo $this->twig->render($this->vues["adminAdministratorsModal"], [
                    'administrator' => $administrator,
                ]);
            }
        }
    }

    function update($param)
    {
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            $_SESSION["error"] = "Méthode non autorisée.";
            header("Location:/admin/administrators");
        } else {
            $id = $_POST['id'];
            $username = $_POST['username'];
            $password = $_POST['password'];
            $username = trim($_POST['username']);
            $password = trim($_POST['password']);
            if (!isset($username) || !isset($password) || empty($username) || empty($password)) {
                $_SESSION["error"] = "Veuillez remplir tous les champs.";
                header("Location:/admin/administrators");
            } else {
                $Admin = [
                    'username' => $username,
                    'password' => $password,
                ];
                if ($this->mdAdministrator->verifyAdministratorByName($Admin) != null) {
                    $_SESSION["error"] = "Cet admin existe déjà.";
                } else {
                    $this->mdAdministrator->updateAdministrator($id, $Admin);
                    header("Location:/admin/administrators");
                }
            }
        }
    }
}
