<?php

namespace controllers;

use \Exception;
use \PDOException;
use models\ModelChapter;

class ControllerAdminChapters
{
    private $mdChapter;

    private $twig;
    private $vues;

    function __construct()
    {
        global $vues, $twig;
        session_start();
        try {
            if ($_SESSION["idAdminConnected"] != null) {
                $this->twig = $twig;
                $this->vues = $vues;

                $this->mdChapter = new ModelChapter();

                $chapters = $this->mdChapter->getChapters();

                echo $twig->render($vues["adminChapters"], [
                    'chapters' => $chapters,
                    'error' => $_SESSION["error"],
                ]);
                $_SESSION["error"] = null;
            } else {
                header("Location:/loginAdmin");
            }
        } catch (PDOException $e) {
            // Gérez les erreurs PDO ici
        } catch (Exception $e2) {
            // Gérez d'autres erreurs ici
        }
    }

    function delete($param)
    {
        $this->mdChapter->deleteChapter($param["id"]);
        header("Location:/admin/chapters");
    }

    function add($param)
    {
        $trimmedName = trim($_POST['name']);
        if (isset($_POST['name']) && !empty($_POST['name']) && !empty($trimmedName)) {
            $name = $_POST['name'];
            $Chapter = [
                'name' => $name,
            ];
            $this->mdChapter->addChapter($Chapter);
            header("Location:/admin/chapters");
        } else {
            $_SESSION["error"] = "Veuillez remplir le champ";
            header("Location:/admin/chapters");
            return;
        }
    }

    function updatemodal($param)
    {
        if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
            $_SESSION["error"] = "Méthode non autorisée.";
            header("Location:/admin/chapters");
        } else {
            $chapter = $this->mdChapter->getChapterByID($param["id"]) ?? null;
            if ($chapter == null) {
                $_SESSION["error"] = "Chapitre introuvable.";
                header("Location:/admin/chapters");
            } else {
                echo $this->twig->render($this->vues["adminChaptersModal"], [
                    'chapter' => $chapter,
                ]);
            }
        }
    }

    function update($param)
    {
        var_dump($_SESSION["error"]);
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            $_SESSION["error"] = "Méthode non autorisée.";
            header("Location:/admin/chapters");
        } else {
            $id = $_POST['id'];
            $name = $_POST['name'];
            $trimmedName = trim($_POST['name']);
            if (!isset($name) || empty($name) || empty($trimmedName)) {
                $_SESSION["error"] = "Veuillez remplir le champ.";
                header("Location:/admin/chapters");
            } else {
                $chapter = $this->mdChapter->verifyChapterByName($name) ?? null;
                if ($chapter != null) {
                    $_SESSION["error"] = "Ce chapitre existe déjà.";
                    header("Location:/admin/chapters");
                } else {
                    $Chapter = [
                        'name' => $name,
                    ];

                    $this->mdChapter->updateChapter($id, $Chapter);
                    header("Location:/admin/chapters");
                }
            }
        }
    }
}
