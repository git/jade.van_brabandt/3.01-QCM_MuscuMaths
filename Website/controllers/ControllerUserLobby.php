<?php

namespace controllers;

use models\ModelLobby;
use \PDOException;
use \Exception;

class ControllerUserLobby
{
    private $mdLobby;

    private $twig;
    private $vues;

    function __construct()
    {
        global $vues, $twig;
        session_start();
        try {
            $this->twig =$twig;
            $this->vues = $vues;

            $this->mdLobby = new ModelLobby();

            $lobbies = $this->mdLobby->getlobbies();

            echo $twig->render($vues["lobby"], [
                'lobbies' => $lobbies,
            ]);
                        
        } catch (PDOException $e) {
            // Gérez les erreurs PDO ici
        } catch (Exception $e2) {
            // Gérez d'autres erreurs ici
        }
    }
}