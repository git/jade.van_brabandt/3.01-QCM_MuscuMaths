<?php

namespace usages;

//préfixe
$rep = __DIR__ . '/../';

//Vues
$vues["singleplayer"]="singleplayer.twig";
$vues["multiplayer"]="multiplayer.twig";
$vues["home"]="home.twig";
$vues["connexion"]="connexion.twig";
$vues["themeChoice"]="themeChoice.twig";
$vues["loginAdmin"]="loginAdmin.twig";
$vues["loginPlayer"]="loginPlayer.twig";
$vues["adminAdministrators"]="adminAdministrators.twig";
$vues["adminAdministratorsModal"]="adminAdministratorsModal.twig";
$vues["adminChapters"]="adminChapters.twig";
$vues["adminChaptersModal"]="adminChaptersModal.twig";
$vues["adminQuestions"]="adminQuestions.twig";
$vues["adminQuestionsModal"]="adminQuestionsModal.twig";
$vues["viewScore"]="viewScore.twig";
$vues["lobby"]="lobby.twig";
$vues["error"]="error.twig";
$vues["userStatus"]="userStatus.twig";
$vues["userPlayerModal"]="userPlayerModal.twig";
