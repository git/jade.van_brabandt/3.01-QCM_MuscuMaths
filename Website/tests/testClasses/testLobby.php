<?php

use PHPUnit\Framework\TestCase;
use classes\Lobby;

final class testLobby extends TestCase
{
    public function testInstanciation()
    {
        $lobby = new Lobby(1, "lobby", "password", 1);
        $this->assertInstanceOf(Lobby::class, $lobby);
        $this->assertEquals(1, $lobby->getId());
        $this->assertEquals("lobby", $lobby->getName());
        $this->assertEquals("password", $lobby->getPassword());
        $this->assertEquals(1, $lobby->getNbPlayers());
    }

    public function testSetter()
    {
        $lobby = new Lobby(1, "lobby", "password", 1);
        $lobby->setNbplayers(2);
        $this->assertEquals(2, $lobby->getNbPlayers());
    }
}
