<?php

use PHPUnit\Framework\TestCase;
use classes\Answer;

final class testAnswer extends TestCase
{
    public function testInstanciation()
    {
        $answer = new Answer(1, "content", 1);
        $this->assertInstanceOf(Answer::class, $answer);
        $this->assertEquals(1, $answer->getId());
        $this->assertEquals("content", $answer->getContent());
        $this->assertEquals(1, $answer->getIdQuestion());
    }

    public function testSetContent()
    {
        $answer = new Answer(1, "content", 1);
        $answer->setContent("new content");
        $this->assertEquals("new content", $answer->getContent());
    }
}
