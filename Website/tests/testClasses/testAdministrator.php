<?php

use PHPUnit\Framework\TestCase;
use classes\Administrator;

final class testAdministrator extends TestCase
{
    public function testInstanciation()
    {
        $admin = new Administrator(1, "admin", "admin");
        $this->assertInstanceOf(Administrator::class, $admin);
        $this->assertEquals(1, $admin->getId());
        $this->assertEquals("admin", $admin->getUsername());
        $this->assertTrue(password_verify("admin", $admin->getHashedPassword()));
    }

    public function testSetHashedPassword()
    {
        $admin = new Administrator(1, "admin", "admin");
        $admin->setHashedPassword(password_hash("bobby", PASSWORD_BCRYPT));
        $this->assertTrue(password_verify("bobby", $admin->getHashedPassword()));
    }
}
