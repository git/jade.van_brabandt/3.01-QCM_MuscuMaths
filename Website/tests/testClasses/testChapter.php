<?php

use PHPUnit\Framework\TestCase;
use classes\Chapter;

final class testChapter extends TestCase
{
    public function testInstanciation()
    {
        $chapter = new Chapter(1, "chapter 1");
        $this->assertInstanceOf(Chapter::class, $chapter);
        $this->assertEquals(1, $chapter->getId());
        $this->assertEquals("chapter 1", $chapter->getName());
    }
}
