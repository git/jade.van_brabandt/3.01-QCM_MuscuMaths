<?php

use PHPUnit\Framework\TestCase;
use classes\Question;

final class testQuestion extends TestCase
{
    public function testInstanciation()
    {
        $question = new Question(1, "question", 1);
        $this->assertInstanceOf(Question::class, $question);
        $this->assertEquals(1, $question->getId());
        $this->assertEquals("question", $question->getContent());
        $this->assertEquals(1, $question->getIdChapter());
    }

    public function testSetter()
    {
        $question = new Question(1, "question", 1);
        $question->setContent("new question");
        $this->assertEquals("new question", $question->getContent());
        $question->setDifficulty(2);
        $this->assertEquals(2, $question->getDifficulty());
        $question->setIdAnswerGood(2);
        $this->assertEquals(2, $question->getIdAnswerGood());
        $question->setNbFails(3);
        $this->assertEquals(3, $question->getNbFails());
    }
}
