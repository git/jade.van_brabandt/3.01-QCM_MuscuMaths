<?php

use PHPUnit\Framework\TestCase;
use classes\Player;

final class testPlayer extends TestCase
{
    public function testInstanciation()
    {
        $player = new Player(1, "player", "player");
        $this->assertInstanceOf(Player::class, $player);
        $this->assertEquals(1, $player->getId());
        $this->assertEquals("player", $player->getNickname());
        $this->assertTrue(password_verify("player", $player->getHashedPassword()));
    }

    public function testSetter()
    {
        $player = new Player(1, "player", "player");
        $player->setHashedPassword("bobby");
        $this->assertTrue(password_verify("bobby", $player->getHashedPassword()));
    }
}
