<?php

use PHPUnit\Framework\TestCase;
use gateways\GatewayJouer;
use gateways\GatewayPlayer;
use gateways\GatewayChapter;

use classes\Player;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertNotEquals;

class testJouer extends TestCase
{
    public function testAddJouer()
    {

        $playerArray = [];
        $playerArray['id'] = 1;
        $playerArray['nickname'] = "testUnit";
        $playerArray['password'] = "testUnit";
        $chapterArray = [];
        $chapterArray['id'] = 1;
        $chapterArray['name'] = "testUnit";
        $gwayChapter = new GatewayChapter();
        $gwayPlayer = new GatewayPlayer();

        $gatewayAnswer = $gwayPlayer->getPlayerByNickname("testUnit");
        if ($gatewayAnswer) {
            $gwayPlayer->deletePlayerByID($gatewayAnswer['id']);
        }

        $gatewayAnswer = $gwayChapter->verifyChapterByName("testUnit");
        if ($gatewayAnswer) {
            $gwayChapter->deleteChapter($gatewayAnswer['id']);
        }

        $gwayChapter->addChapter($chapterArray);
        $gwayPlayer->addPlayer($playerArray);
        $playerFromGWay = $gwayPlayer->getPlayerByNickname("testUnit");
        $chapterFromGWay = $gwayChapter->verifyChapterByName("testUnit");

        $gway = new GatewayJouer();
        $gway->addJouer(
            array(
                'idchapter' => $chapterFromGWay['id'],
                'idplayer' => $playerFromGWay['id'],
                'maxscore' => 0
            )
        );
        $maxScore = $gway->getMaxScoreByPlayerAndChapter(
            array(
                'idchapter' => $chapterFromGWay['id'],
                'idplayer' => $playerFromGWay['id']
            )
        );
        assertEquals(0, $maxScore['maxscore']);

        $gway->updateJouer(
            array(
                'idchapter' => $chapterFromGWay['id'],
                'idplayer' => $playerFromGWay['id'],
                'maxscore' => 10
            )
        );
        $maxScore = $gway->getMaxScoreByPlayerAndChapter(
            array(
                'idchapter' => $chapterFromGWay['id'],
                'idplayer' => $playerFromGWay['id']
            )
        );
        assertEquals(10, $maxScore['maxscore']);

        $jouer = $gway->verifyJouer(
            array(
                'idchapter' => $chapterFromGWay['id'],
                'idplayer' => $playerFromGWay['id']
            )
        );
        assertEquals($chapterFromGWay['id'], $jouer['idchapter']);
        assertEquals($playerFromGWay['id'], $jouer['idplayer']);

        $player = new Player($playerFromGWay['id'], $playerFromGWay['nickname'], "testUnit");
        $maxScoreAndChapter = $gway->getMaxScoresWithChapter($player);
        assertEquals(10, $maxScoreAndChapter[0]['maxscore']);
        assertEquals($chapterFromGWay['id'], $maxScoreAndChapter[0]['idchapter']);

        $gway->deleteJouer(
            array(
                'idchapter' => $chapterFromGWay['id'],
                'idplayer' => $playerFromGWay['id']
            )
        );
        $jouer = $gway->verifyJouer(
            array(
                'idchapter' => $chapterFromGWay['id'],
                'idplayer' => $playerFromGWay['id']
            )
        );
        assertEquals(false, $jouer);
        $gwayPlayer->deletePlayerByID($playerFromGWay['id']);
        $gwayChapter->deleteChapter($chapterFromGWay['id']);
    }

}