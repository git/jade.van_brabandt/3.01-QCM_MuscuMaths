<?php

use PHPUnit\Framework\TestCase;
use gateways\GatewayQuestion;
use gateways\GatewayAnswer;

use classes\Question;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertNotEquals;

class testQuestion extends TestCase
{
    public function testQuestions()
    {

        $question = [];
        $question['id'] = 1;
        $question['content'] = "test Unit_";
        $question['idchapter'] = 1;
        $question['difficulty'] = 1;
        $question['nbfails'] = 0;

        $gateway = new GatewayQuestion();
        $lenght = count($gateway->getQuestions());
        $lenght2 = count($gateway->getQuestionsByChapterAndDifficulty(1, 1));
        $question['id'] = $gateway->addQuestion($question);

        $gwayAnswer = new GatewayAnswer();
        $answer = array(
            'content' => 'This is a test answer',
            'idquestion' => $question['id']
        );
        $answer2 = array(
            'content' => 'This is a test answer 2',
            'idquestion' => $question['id']
        );
        $answer3 = array(
            'content' => 'This is a test answer 3',
            'idquestion' => $question['id']
        );
        $answer4 = array(
            'content' => 'This is a test answer 4',
            'idquestion' => $question['id']
        );
        $answerId1 = $gwayAnswer->addAnswer($answer);
        $answerId2 = $gwayAnswer->addAnswer($answer2);
        $answerId3 = $gwayAnswer->addAnswer($answer3);
        $answerId4 = $gwayAnswer->addAnswer($answer4);
        $question['idanswergood'] = $answerId1;

        $question2 = $gateway->getQuestionByID($question['id']);
        assertEquals($question['content'], $question2['content']);
        assertEquals($lenght + 1, count($gateway->getQuestions()));
        assertEquals($lenght2 + 1, count($gateway->getQuestionsByChapterAndDifficulty(1, 1)));


        $question['content'] = "test Unit 2_";
        $gateway->updateQuestion($question['id'], $question);
        $question2 = $gateway->getQuestionByID($question['id']);
        assertEquals($question['content'], $question2['content']);

        $question['difficulty'] = 2;
        $questionInstance = new Question(
            $question['id'],
            $question['content'],
            $question['idchapter'],
            $question['difficulty'],
            $question['nbfails']
        );
        $questionInstance->setDifficulty($question['difficulty']);
        $gateway->updateDifficulty($questionInstance);
        $question3 = $gateway->getQuestionByID($question['id']);
        assertEquals($question['difficulty'], $question3['difficulty']);

        $question['nbfails'] = 1;
        $questionInstance = new Question(
            $question['id'],
            $question['content'],
            $question['idchapter'],
            $question['difficulty'],
            $question['nbfails']
        );
        $questionInstance->setNbFails($question['nbfails']);
        $gateway->updateNbFails($questionInstance);
        $question2 = $gateway->getQuestionByID($question['id']);
        assertEquals($question['nbfails'], $question2['nbfails']);


        $gateway->deleteQuestionByID($question['id']);
        assertEquals($lenght, count($gateway->getQuestions()));
        $gwayAnswer->deleteAnswer($answerId1);
        $gwayAnswer->deleteAnswer($answerId2);
        $gwayAnswer->deleteAnswer($answerId3);
        $gwayAnswer->deleteAnswer($answerId4);

    }
}