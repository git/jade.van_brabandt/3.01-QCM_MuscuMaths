<?php

use PHPUnit\Framework\TestCase;
use gateways\GatewayLobby;
use classes\Lobby;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertNotEquals;

class testLobby extends TestCase
{
    public function testAddLobby()
    {
        $lobby = new Lobby(1, "testUnit", "testUnit", 1);
        $gateway = new GatewayLobby();
        $gateway->deleteLobby($gateway->getLobbyByName("testUnit"));
        $gateway->addLobby($lobby);
        $lobbyArray = $gateway->getLobbyByName("testUnit");
        $lobby2 = new Lobby($lobbyArray['id'], $lobbyArray['name'], $lobbyArray['password'], $lobbyArray['nbplayers']);
        assertEquals($lobby->getId(), $lobby2->getId());
        assertEquals($lobby->getName(), $lobby2->getName());
        assertEquals($lobby->getPassword(), $lobby2->getPassword());
        assertEquals($lobby->getNbPlayers(), $lobby2->getNbPlayers());
        $gateway->deleteLobby($lobby->getId());
    }

    public function testGetLobbies()
    {
        $gateway = new GatewayLobby();
        $lobbies = $gateway->getLobbies();
        assertNotEquals(0, count($lobbies));
    }

    public function testGetLobbyByName()
    {
        $lobby = new Lobby(1, "testUnit", "testUnit", 1);
        $gateway = new GatewayLobby();
        $gateway->deleteLobby($gateway->getLobbyByName("testUnit"));
        $gateway->addLobby($lobby);
        $lobbyArray = $gateway->getLobbyByName("testUnit");
        $lobby2 = new Lobby(
            $lobbyArray['id'],
            $lobbyArray['name'],
            $lobbyArray['password'],
            $lobbyArray['nbplayers']
        );
        assertEquals($lobby->getId(), $lobby2->getId());
        assertEquals($lobby->getName(), $lobby2->getName());
        assertEquals($lobby->getPassword(), $lobby2->getPassword());
        assertEquals($lobby->getNbPlayers(), $lobby2->getNbPlayers());
        $gateway->deleteLobby($lobby->getId());
    }

    public function testDeleteLobby()
    {
        $lobby = new Lobby(1, "testUnit", "testUnit", 1);
        $gateway = new GatewayLobby();
        $gateway->deleteLobby($gateway->getLobbyByName("testUnit"));
        $gateway->addLobby($lobby);
        $gateway->deleteLobby($lobby->getId());
        $lobby2 = $gateway->getLobbyByName("testUnit");
        assertEquals(false, $lobby2);
    }
}