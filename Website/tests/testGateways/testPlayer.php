<?php

use PHPUnit\Framework\TestCase;
use gateways\GatewayPlayer;
use classes\Player;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertNotEquals;

class testPlayer extends TestCase
{
    public function testAddPlayer()
    {
        $player = [];
        $player['id'] = 1;
        $player['nickname'] = "testUnit";
        $player['password'] = "testUnit";
        $gateway = new GatewayPlayer();
        $gatewayAnswer = $gateway->getPlayerByNickname("testUnit");
        if ($gatewayAnswer) {
            $gateway->deletePlayerByID($gatewayAnswer['id']);
        }
        $gateway->addPlayer($player);
        $player2 = $gateway->getPlayerByNickname("testUnit");
        assertEquals($player['nickname'], $player2['nickname']);
        $gateway->deletePlayerByID($player['id']);
    }

    public function testGetPlayers()
    {
        $gateway = new GatewayPlayer();
        $players = $gateway->getPlayers();
        assertNotEquals(0, count($players));
    }

    public function testGetPlayerByNickname()
    {
        $player = [];
        $player['id'] = 1;
        $player['nickname'] = "testUnit";
        $player['password'] = "testUnit";
        $gateway = new GatewayPlayer();
        $gatewayAnswer = $gateway->getPlayerByNickname("testUnit");
        if ($gatewayAnswer) {
            $gateway->deletePlayerByID($gatewayAnswer['id']);
        }
        $gateway->addPlayer($player);
        $player2 = $gateway->getPlayerByNickname("testUnit");
        assertEquals($player['nickname'], $player2['nickname']);
        $gateway->deletePlayerByID($player['id']);
    }

    public function testDeletePlayer()
    {
        $player = [];
        $player['id'] = 1;
        $player['nickname'] = "testUnit";
        $player['password'] = "testUnit";
        $gateway = new GatewayPlayer();
        $gatewayAnswer = $gateway->getPlayerByNickname("testUnit");
        if ($gatewayAnswer) {
            $gateway->deletePlayerByID($gatewayAnswer['id']);
        }
        $gateway->addPlayer($player);
        $gatewayAnswer = $gateway->getPlayerByNickname("testUnit");
        if ($gatewayAnswer) {
            $gateway->deletePlayerByID($gatewayAnswer['id']);
        }
        $player2 = $gateway->getPlayerByNickname("testUnit");
        assertEquals(false, $player2);
    }
}