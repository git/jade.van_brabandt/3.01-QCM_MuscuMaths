<?php

use PHPUnit\Framework\TestCase;
use gateways\GatewayAdministrator;

use function PHPUnit\Framework\assertEquals;

final class testAdministrators extends TestCase
{
    public function testGetAdministratorByUsername()
    {
        $gateway = new GatewayAdministrator();
        $response = $gateway->getAdministratorByUsername("admin");
        assertEquals($response['username'], "admin");
        assertEquals($response['id'], "42");
    }

    public function testGetAdministratorByID()
    {
        $gateway = new GatewayAdministrator();
        $response = $gateway->getAdministratorByID(42);
        assertEquals($response['id'], "42");
        assertEquals($response['username'], "admin");
    }

    public function testGetAdministrators()
    {
        $gateway = new GatewayAdministrator();
        $response = $gateway->getAdministrators();
        assertEquals($response[0]['id'], "42");
        assertEquals($response[0]['username'], "admin");
        assertEquals($response[1]['id'], "121");
        assertEquals($response[1]['username'], "testadmin");
    }

    public function testUpdateAdministrator()
    {
        $gateway = new GatewayAdministrator();
        $gateway->updateAdministrator(1, ["username" => "test", "password" => "test"]);
    }

    public function testAddAndDeleteAdministrator()
    {

        $gateway = new GatewayAdministrator();
        $gateway->addAdministrator(["username" => "test", "password" => "test"]);
        $response = $gateway->getAdministratorByUsername("test");
        assertEquals($response['username'], "test");
        $gateway->deleteAdministratorByID($gateway->getAdministratorByUsername("test")['id']);
    }

    public function testVerifyAdministrator()
    {
        $gateway = new GatewayAdministrator();
        $response = $gateway->verifyAdministrator(["username" => "testadmin", "password" => '123456']);
        assertEquals(121, $response['id']);
    }
}