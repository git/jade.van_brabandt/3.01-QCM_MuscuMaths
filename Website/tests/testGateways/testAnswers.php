<?php

use PHPUnit\Framework\TestCase;
use gateways\GatewayAnswer;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertNotEquals;

class testAnswers extends TestCase
{
    public function testAddAnswer()
    {
        $gateway = new GatewayAnswer();
        $answer = array(
            'content' => 'This is a test answer',
            'idquestion' => 2
        );
        $answerId = $gateway->addAnswer($answer);
        assertNotEquals($answerId, null);
        $gateway->deleteAnswer($answerId);
        $answerId = $gateway->getAnswerByID($answerId);
        assertEquals($answerId, null);
        $gateway->deleteAnswer($answerId);
    }

    public function testGetAnswerByID()
    {
        $gateway = new GatewayAnswer();
        $answer = $gateway->getAnswerByID(5);
        assertEquals($answer['content'], '4');
    }

    public function testGetAnswersByIDQuestions()
    {
        $gateway = new GatewayAnswer();
        $answers = $gateway->getAnswersByIDQuestions(2);
        assertEquals($answers[0]['content'], '4');
        assertEquals($answers[1]['content'], '-4');
        assertEquals($answers[2]['content'], 'on ne peut pas simplifier');
        assertEquals($answers[3]['content'], '1');
    }

    public function testUpdateAnswer()
    {
        $gateway = new GatewayAnswer();
        $answer = array(
            'content' => 'This is a test answer',
            'idquestion' => 2
        );
        $answerId = $gateway->addAnswer($answer);
        $answer = array(
            'id' => $answerId,
            'content' => 'This is a test answer updated',
            'idquestion' => 2
        );
        $gateway->updateAnswer($answerId, $answer);
        $answer = $gateway->getAnswerByID($answerId);
        assertEquals($answer['content'], 'This is a test answer updated');
        $gateway->deleteAnswer($answerId);
    }
}