<?php

use PHPUnit\Framework\TestCase;
use gateways\GatewayChapter;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertNotEquals;

class testChapters extends TestCase
{
    public function testAddChapter()
    {
        $gateway = new GatewayChapter();
        $chapter = array(
            'name' => 'This is a test chapter',
            'idcourse' => 1
        );
        $chapterId = $gateway->addChapter($chapter);
        assertNotEquals($chapterId, null);
        $gateway->deleteChapter($chapterId);
        $chapter = $gateway->getChapterByID($chapterId);
        assertEquals($chapter, null);
    }

    public function testGetChapters()
    {
        $gateway = new GatewayChapter();
        $chapters = $gateway->getChapters();
        assertEquals($chapters[0]['name'], 'Calculs algébrique et littéral ');
    }

    public function testUpdateChapter()
    {
        $gateway = new GatewayChapter();
        $chapter = array(
            'name' => 'This is a test chapter',
            'idcourse' => 1
        );
        $chapterId = $gateway->addChapter($chapter);
        $chapter = array(
            'id' => $chapterId,
            'name' => 'This is a test chapter updated',
            'idcourse' => 1
        );
        $gateway->updateChapter($chapterId, $chapter);
        $chapter = $gateway->getChapterByID($chapterId);
        assertEquals($chapter['name'], 'This is a test chapter updated');
        $gateway->deleteChapter($chapterId);
    }

    public function testVerifyChapter()
    {
        $gateway = new GatewayChapter();
        $chapter = array(
            'name' => 'This is a test chapter',
            'idcourse' => 1
        );
        $chapterId = $gateway->addChapter($chapter);
        $chapter = $gateway->getChapterByID($chapterId);
        assertEquals($gateway->verifyChapterByID($chapterId)['id'], $chapterId);
        assertEquals($gateway->verifyChapterByName("This is a test chapter")['id'], $chapterId);
        $gateway->deleteChapter($chapterId);
    }

}