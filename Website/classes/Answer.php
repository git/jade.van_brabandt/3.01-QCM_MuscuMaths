<?php
namespace classes;

class Answer
{
    private int $id;
    private string $content;
    private int $idQuestion;

    public function __construct(int $id, string $content, int $idQuestion)
    {
        $this->id = $id;
        $this->content = $content;
        $this->idQuestion = $idQuestion;
    }

    public function getId(): int
    {
        return $this->id;
    }
    public function getContent()
    {
        return $this->content;
    }

    public function getIdQuestion()
    {
        return $this->idQuestion;
    }

    public function setContent(string $content)
    {
        $this->content = $content;
    }
}
