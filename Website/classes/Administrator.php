<?php
namespace classes;

use \Exception;

class Administrator
{
    private int $id;
    private string $username;
    private string $hashedPassword;

    public function __construct(int $id, string $username, string $password)
    {
        $this->id = $id;
        $this->username = $username;
        try {
            $this->hashedPassword = password_hash($password, PASSWORD_BCRYPT);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getHashedPassword()
    {
        return $this->hashedPassword;
    }

    public function setHashedPassword(string $hashedPassword)
    {
        $this->hashedPassword = $hashedPassword;
    }
}
