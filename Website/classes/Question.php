<?php
namespace classes;

class Question
{
    private int $id;
    private string $content;
    private int $idChapter;
    private int $difficulty;
    private int $nbFails;
    private int $idAnswerGood;

    public function __construct(
        int $id,
        string $content,
        int $idChapter,
        int $idAnswerGood = -1,
        int $difficulty = 1,
        int $nbFails = 0
    ) {
        $this->id = $id;
        $this->content = $content;
        $this->idChapter = $idChapter;
        $this->idAnswerGood = ($idAnswerGood !== -1) ? $idAnswerGood : -1;
        $this->difficulty = $difficulty;
        $this->nbFails = $nbFails;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getIdChapter(): int
    {
        return $this->idChapter;
    }

    public function getIdAnswerGood(): int
    {
        return $this->idAnswerGood;
    }

    public function getDifficulty(): int
    {
        return $this->difficulty;
    }

    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    public function setIdAnswerGood(int $idAnswerGood): void
    {
        $this->idAnswerGood = $idAnswerGood;
    }

    public function setDifficulty(int $difficulty): void
    {
        $this->difficulty = $difficulty;
    }

    public function getNbFails(): int
    {
        return $this->nbFails;
    }

    public function setNbFails(int $nbFails): void
    {
        $this->nbFails = $nbFails;
    }
}