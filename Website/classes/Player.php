<?php
namespace classes;

use \Exception;

class Player
{
    private int $id;
    private string $nickname;
    private string $hashedPassword;

    public function __construct(int $id, string $nickname, string $password)
    {
        $this->id = $id;
        $this->nickname = $nickname;
        try {
            $this->hashedPassword = password_hash($password, PASSWORD_BCRYPT);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getNickname()
    {
        return $this->nickname;
    }

    public function getHashedPassword()
    {
        return $this->hashedPassword;
    }
    public function setHashedPassword(string $password)
    {
        $this->hashedPassword = password_hash($password, PASSWORD_BCRYPT);
    }
}
