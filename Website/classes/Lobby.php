<?php
namespace classes;

class Lobby
{
    private int $id;
    private string $name;
    private string $password;
    private int $nbPlayers;

    public function __construct(int $id, string $name, string $password, int $nbPlayers)
    {
        $this->id = $id;
        $this->name = $name;
        $this->password = $password;
        $this->nbPlayers = $nbPlayers;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getNbPlayers()
    {
        return $this->nbPlayers;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setNbplayers(int $nbPlayers)
    {
        $this->nbPlayers = $nbPlayers;
    }
}
