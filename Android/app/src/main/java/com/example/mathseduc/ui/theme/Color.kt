package com.example.mathseduc.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)

object Colors {
    val Purple200 = Color(0xFFBB86FC)
    val Purple500 = Color(0xFF6200EE)
    val Purple700 = Color(0xFF3700B3)
    val Teal200 = Color(0xFF03DAC5)
    val Teal700 = Color(0xFF018786)
    val Black = Color(0xFF000000)
    val White = Color(0xFFFFFFFF)
    val Green = Color(0xFF008000)
    val Orange = Color(0xFFFFA500)
    val Grey = Color(0xFF8C92AC)
    val Blue = Color(0xFF0D6EFD)
    val Red = Color(0xFFFF0000)
    val BlackGrey = Color(0xFF5C636A)
    val Cyan = Color(0xFF00FFFF)
    val Turquoise = Color(0xFF40E0D0)
}