package com.example.mathseduc.ui

import android.app.Activity
import android.content.Context
import android.content.res.Configuration
import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalView
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import com.example.mathseduc.MainActivity
import com.example.mathseduc.controllers.ControllerChapter
import com.example.mathseduc.controllers.ControllerLobby
import com.example.mathseduc.controllers.ControllerUtiliser
import com.example.mathseduc.models.Lobby
import com.example.mathseduc.ui.theme.Colors
import com.example.mathseduc.viewModel.MultiPageViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MultipartBody

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MultiPage(navController: NavController) {
    val context = LocalContext.current
    val activity = LocalView.current.context as Activity

    val viewModel = ViewModelProvider(navController.getViewModelStoreOwner(navController.graph.id)).get(MultiPageViewModel::class.java)
    val lobbyList by viewModel.lobbyList.collectAsState(initial = emptyList())

    var selectedItem : Lobby? = null
    val scope = rememberCoroutineScope()

    val isPortrait = LocalConfiguration.current.orientation == Configuration.ORIENTATION_PORTRAIT

    WindowCompat.setDecorFitsSystemWindows(activity.window, false)

    val windowInsetsController = remember {
        WindowCompat.getInsetsController(activity.window, activity.window.decorView)
    }

    windowInsetsController.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE

    // Hide the status bar and navigation bar
    windowInsetsController.hide(WindowInsetsCompat.Type.systemBars())


    // Fonction pour actualiser la liste des lobbies
    suspend fun refreshLobbyList() {
        viewModel.updateLobbyList()
    }

    DisposableEffect(Unit) {
        val refreshRunnable = {
            scope.launch(Dispatchers.IO) {
                try {
                    refreshLobbyList()
                } catch (e: Exception) {
                    Log.e("MainActivity", "Error refreshing lobbies: ${e.message}")
                }
            }
        }
        refreshRunnable.invoke()

        onDispose {
        }
    }

    TopAppBar(
            colors = TopAppBarDefaults.topAppBarColors(
                    containerColor = Color.Transparent,
            ),
            title = {},
            navigationIcon = {
                IconButton(
                        onClick = { navController.navigate("home") },
                        modifier = Modifier.size(60.dp)
                ) {
                    Icon(
                            imageVector = Icons.Filled.ArrowBack,
                            contentDescription = "Retour",
                            modifier = Modifier.size(36.dp),
                            tint = Color.White
                    )
                }
            },
    )
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(start = 16.dp, top = 62.dp, end = 16.dp)
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = 10.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    text = "Liste des lobbies",
                    color = Colors.White,
                        fontSize = 17.sp,
                    modifier = Modifier
                        .weight(if (isPortrait) 1f else 0.6f)
                        .padding(start = 25.dp)
                )
                Button(
                    onClick = {
                        navController.navigate("createLobby")
                    },
                    shape = RoundedCornerShape(15),
                    modifier = Modifier
                        .weight(if (isPortrait) 1f else 0.4f)
                        .padding(end = 20.dp),
                    colors = ButtonDefaults.buttonColors(Colors.Blue)
                ) {
                    Text(text = "Ajouter un lobby")
                }
            }

            LazyColumn(
                modifier = Modifier
                    .fillMaxSize()
                    .border(2.dp, Color.White, shape = RoundedCornerShape(3))
                    .weight(0.85f),
            ) {
                items(lobbyList) { lobby ->
                    LobbyItem(lobby, selectedItem == lobby) {
                        selectedItem = it

                        if (viewModel.getNbPlayerInLobby(selectedItem!!) < selectedItem!!.nbplayers) {
                            // Créer un Utiliser si le lobby n'est pas plein

                            scope.launch {
                                createUtiliserForLobby(context, selectedItem!!, navController)
                            }
                        } else {
                            // Afficher un message Toast si le lobby est plein
                            Toast.makeText(
                                context,
                                "Oh nan, le serveur est déjà plein ! Réessayer plus tard.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }
            }

            Button(
                onClick = {
                    scope.launch {
                        refreshLobbyList()
                    }
                },
                shape = RoundedCornerShape(15),
                modifier = Modifier
                    .fillMaxWidth(),
                colors = ButtonDefaults.buttonColors(Colors.Blue)
            ) {
                Text(text = "Actualiser")
            }
        }
    }


private suspend fun createUtiliserForLobby(context: Context, lobby: Lobby, navController: NavController) {
    withContext(Dispatchers.IO) {
        val formDataBuilder = MultipartBody.Builder().setType(MultipartBody.FORM)
        formDataBuilder.addFormDataPart("idplayer", MainActivity.idPlayerConnected.toString())
        formDataBuilder.addFormDataPart("idlobby", lobby.id.toString())
        formDataBuilder.addFormDataPart("playertime", "0")

        ControllerUtiliser.createUtiliserByIdLobby(formDataBuilder)

        // Naviguer vers l'activité ServerDetails avec les détails du lobby
        val mainScope = MainScope()
        mainScope.launch {
            navController.navigate("serverDetails/${lobby.name}/${lobby.id}/${lobby.idchapter}/${lobby.nbplayers}/${lobby.difficulty}")
        }
    }
}

@Composable
fun LobbyItem(lobby: Lobby, isSelected: Boolean, onItemClick: (Lobby) -> Unit) {
    val context = LocalContext.current
    var showDialog by rememberSaveable { mutableStateOf(false) }
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
            .clickable {
                if (lobby.password.isNotEmpty()) {
                    showDialog = true
                } else {
                    onItemClick(lobby)
                }
            }
            .clip(RoundedCornerShape(8.dp))
            .background(if (isSelected) Colors.Orange else Colors.Grey)
            .padding(16.dp)
    ) {
        Text(
            text = lobby.name,
            fontSize = 18.sp,
            color = Colors.White,
            modifier = Modifier.weight(1f)
        )
        Text(
            text = "${ControllerLobby.getNbPlayerInLobby(lobby.id)}/${lobby.nbplayers}",
            fontSize = 18.sp,
            color = (if(ControllerLobby.getNbPlayerInLobby(lobby.id)==lobby.nbplayers) Color.Red else Color.White),
            modifier = Modifier.weight(1f)
        )
        Text(
            text = ControllerChapter.getChapterNameById(lobby.idchapter).toString(),
            fontSize = 18.sp,
            color = Colors.White,
            modifier = Modifier.weight(3f)
        )
        Text(
            text = lobby.difficulty.toString(),
            fontSize = 18.sp,
            color = Colors.White,
            modifier = Modifier.weight(1f)
        )

        if (showDialog) {
            ConfirmDialog(
                    onDismiss = { showDialog = false },
                onConfirm = { password ->
                    println("Password input : $password")
                    if (password == lobby.password) {
                    showDialog = false
                    onItemClick(lobby)
                } else {
                    Toast.makeText(context, "Wrong password!", Toast.LENGTH_SHORT).show()
                }
            }
            )
        }
    }
}

@Composable
fun ConfirmDialog(onDismiss: () -> Unit, onConfirm: (String) -> Unit) {
    val context = LocalContext.current

    var password by remember { mutableStateOf("") }

    AlertDialog(
            onDismissRequest = {
                onDismiss()
            },
            title = { Text("Enter Password") },
            confirmButton = {
                Button(
                        onClick = {
                            onConfirm(password)
                        }
                ) {
                    Text("Confirm")
                }
            },
            dismissButton = {
                Button(
                        onClick = {
                            onDismiss()
                        }
                ) {
                    Text("Dismiss")
                }
            },
            text = {
                Column(
                        modifier = Modifier
                                .fillMaxWidth()
                                .padding(8.dp)
                ) {

                    OutlinedTextField(
                            value = password,
                            onValueChange = { password = it },
                            label = { Text("Password") },
                            visualTransformation = PasswordVisualTransformation(),
                            keyboardOptions = KeyboardOptions.Default.copy(
                                    keyboardType = KeyboardType.Password
                            ),
                            modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(8.dp)
                    )
                }
            }
    )
}