package com.example.mathseduc

import android.app.Activity
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalView
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.example.mathseduc.controllers.ControllerPlayer


class ConnexionPlayerActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val navController = rememberNavController()
            ConnexionPlayerContent(navController = navController)
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ConnexionPlayerContent(navController: NavController) {
    var nickname by rememberSaveable { mutableStateOf("") }
    var password by rememberSaveable { mutableStateOf("") }
    var showDialog by rememberSaveable { mutableStateOf(false) }
    val activity = LocalView.current.context as Activity

    val context = LocalContext.current
    val isPortrait = LocalConfiguration.current.orientation == Configuration.ORIENTATION_PORTRAIT

    WindowCompat.setDecorFitsSystemWindows(activity.window, false)

    val windowInsetsController = remember {
        WindowCompat.getInsetsController(activity.window, activity.window.decorView)
    }

    // Hide the status bar
    windowInsetsController.hide(WindowInsetsCompat.Type.statusBars())

    TopAppBar(
                colors = TopAppBarDefaults.topAppBarColors(
                        containerColor = Color.Transparent,
                ),
                title = {},
                navigationIcon = {
                    IconButton(
                            onClick = { navController.navigate("home") },
                            modifier = Modifier.size(60.dp)
                    ) {
                        Icon(
                                imageVector = Icons.Filled.ArrowBack,
                                contentDescription = "Retour",
                                modifier = Modifier.size(36.dp),
                                tint = Color.White
                        )
                    }
                },
        )
        Column(
                modifier = Modifier
                        .fillMaxSize()
                        .padding(16.dp,0.dp),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
        ) {

            Image(
                    painter = painterResource(id = R.drawable.logo),
                    contentDescription = null,
                    modifier = Modifier.padding(0.dp, 2.dp, 0.dp)
                            .size(if (isPortrait) 120.dp else 90.dp)
            )

            Spacer(modifier = Modifier.height(if (isPortrait) 16.dp else 4.dp))

            OutlinedTextField(
                    value = nickname,
                    onValueChange = { nickname = it },
                    label = { Text("Nickname") },
                    modifier = Modifier
                            .fillMaxWidth(if (isPortrait) 1f else 0.5f)
                            .padding(if (isPortrait) 8.dp else 8.dp,3.dp),
                    colors = OutlinedTextFieldDefaults.colors(
                            focusedContainerColor = Color.White,
                            unfocusedContainerColor = Color.White,
                            disabledContainerColor = Color.White,
                            focusedBorderColor = Color.Blue,
                    ),
                    shape = RoundedCornerShape(8.dp)
            )

            OutlinedTextField(
                    value = password,
                    onValueChange = { password = it },
                    label = { Text("Password") },
                    visualTransformation = PasswordVisualTransformation(),
                    keyboardOptions = KeyboardOptions.Default.copy(
                            keyboardType = KeyboardType.Password
                    ),
                    modifier = Modifier
                            .fillMaxWidth(if (isPortrait) 1f else 0.5f)
                            .padding(8.dp),
                    colors = OutlinedTextFieldDefaults.colors(
                            focusedContainerColor = Color.White,
                            unfocusedContainerColor = Color.White,
                            disabledContainerColor = Color.White,
                            focusedBorderColor = Color.Blue,
                    ),
                    shape = RoundedCornerShape(8.dp)
            )

            Spacer(modifier = Modifier.height(if (isPortrait) 16.dp else 7.dp))

        Button(
            onClick = {
                val isAuthenticated = ControllerPlayer.authenticateUser(nickname, password)
                if (isAuthenticated != -1) {
                    MainActivity.idPlayerConnected = isAuthenticated
                    Toast.makeText(context, "Connexion réussie, bienvenue $nickname !", Toast.LENGTH_SHORT).show()
                    navController.navigate("home")
                } else {
                    Toast.makeText(context, "Connexion échouée. Veuillez réessayer.", Toast.LENGTH_SHORT).show()
                    nickname = ""
                    password = ""
                }
            },
            modifier = Modifier
                    .width(230.dp)
                .height(48.dp)
        ) {
            Text("Login")
        }

            Spacer(modifier = Modifier.height(if (isPortrait) 16.dp else 10.dp))

            Button(
                    onClick = { showDialog = true },
                    modifier = Modifier
                            .width(230.dp)
                            .height(48.dp),
                    colors = ButtonDefaults.buttonColors(
                            containerColor = Color(0xFF40E0D0))
            ) {
                Text("Register")
            }

            if (showDialog) {
                RegisterDialog(onDismiss = { showDialog = false })
            }
        }
    }


@Composable
fun RegisterDialog(onDismiss: () -> Unit) {
    val context = LocalContext.current

    var nickname by remember { mutableStateOf("") }
    var password by remember { mutableStateOf("") }

    var nicknameError by remember { mutableStateOf(false) }
    var passwordError by remember { mutableStateOf(false) }

    AlertDialog(
            onDismissRequest = {
                onDismiss()
            },
            title = { Text("Register") },
            confirmButton = {
                Button(
                        onClick = {
                            if (nickname.isNotBlank() && password.isNotBlank()) {
                                val playerId = ControllerPlayer.createPlayer(nickname, password)
                                onDismiss()
                            } else {
                                if (nickname.isBlank()) nicknameError = true
                                if (password.isBlank()) passwordError = true
                            }
                        }
                ) {
                    Text("Save")
                }
            },
            dismissButton = {
                Button(
                        onClick = {
                            onDismiss()
                        }
                ) {
                    Text("Dismiss")
                }
            },
            text = {
                Column(
                        modifier = Modifier
                                .fillMaxWidth()
                                .padding(8.dp)
                ) {
                    OutlinedTextField(
                            value = nickname,
                            onValueChange = {
                                nickname = it
                                nicknameError = false
                            },
                            label = { Text("Nickname") },
                            modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(8.dp),
                            isError = nicknameError
                    )

                    OutlinedTextField(
                            value = password,
                            onValueChange = {
                                password = it
                                passwordError = false
                            },
                            label = { Text("Password") },
                            visualTransformation = PasswordVisualTransformation(),
                            keyboardOptions = KeyboardOptions.Default.copy(
                                    keyboardType = KeyboardType.Password
                            ),
                            modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(8.dp),
                            isError = passwordError
                    )

                    if (nicknameError || passwordError) {
                        Text(
                                text = "Please fill in all fields",
                                color = Color.Red,
                                modifier = Modifier.padding(start = 8.dp)
                        )
                    }
                }
            }
    )
}
