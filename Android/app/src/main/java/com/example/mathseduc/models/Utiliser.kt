package com.example.mathseduc.models
data class Utiliser (
    val idlobby: Int,
    val idplayer: Int,
    val playertime: Int
)
