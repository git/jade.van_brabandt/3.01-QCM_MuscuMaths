package com.example.mathseduc.controllers

import Answer
import android.annotation.SuppressLint
import android.os.StrictMode
import com.example.mathseduc.models.Question
import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONArray


class ControllerQuestion {
    companion object {
        fun getQuestionsForLobby(questionIds: ArrayList<String>): List<Question>? {
            val questions = ArrayList<Question>()

            for (questionId in questionIds) {
                val question = getQuestionById(questionId)
                if (question != null) {
                    questions.add(question)
                }
            }

            return if (questions.isNotEmpty()) questions else null
        }

        private fun getQuestionById(questionId: String): Question? {
            val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
            StrictMode.setThreadPolicy(policy)

            // Client HTTP API
            val client = OkHttpClient()

            // API Access
            val request = Request.Builder()
                .url("https://trusting-panini.87-106-126-109.plesk.page/api/questions/$questionId/qUOGkWdoPCgbmuqxIC8xiaX0rV1Pw1LoPafkaoHOgszEyD9P2vcOu493xCDZpAqO")
                .build()

            // API Response
            client.newCall(request).execute().use { response ->
                if (!response.isSuccessful) {
                    // Handle error, log, or throw an exception as needed
                    return null
                }

                // Gson deserialization
                val gson = Gson()
                val responseData = response.body?.string() ?: return null
                val jsonArray = JSONArray(responseData)

                val answers = mutableListOf<Answer>()
                for (i in 0 until jsonArray.length()) {
                    val jsonObject = jsonArray.getJSONObject(i)
                    val answer = Answer(
                        id = jsonObject.getInt("a_id"),
                        content = jsonObject.getString("a_content")
                    )
                    answers.add(answer)
                }

                val questions = mutableListOf<Question>()
                val jsonObject = jsonArray.getJSONObject(0)
                val question = Question(
                    id = jsonObject.getInt("q_id"),
                    content = jsonObject.getString("q_content"),
                    nbfails = jsonObject.getInt("nbfails"),
                    difficulty = jsonObject.getInt("difficulty"),
                    idchapter = jsonObject.getInt("idchapter"),
                    idanswergood = jsonObject.getInt("idanswergood"),
                    answers = answers
                )
                questions.add(question)

                return if (questions.isNotEmpty()) questions[0] else null
            }
        }
    }
}