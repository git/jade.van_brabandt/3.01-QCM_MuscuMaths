package com.example.mathseduc.models

data class Chapter(
    val id: String,
    val name: String,
)