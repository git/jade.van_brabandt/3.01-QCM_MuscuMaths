package com.example.mathseduc

import android.os.Bundle
import android.os.CountDownTimer
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import com.example.mathseduc.ui.theme.MathsEducTheme


class QuizMultiActivity : ComponentActivity() {
    companion object {
        var countDownTimer: CountDownTimer? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            MathsEducTheme {
                /*
                val lobbyId = intent.getIntExtra("lobbyId",-1)
                val serverName = intent.getStringExtra("serverName")

                QuizMultiScreen(lobbyId, serverName!!) //TODO sus
                 */
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        // Stop the CountDownTimer to avoid memory leaks
        countDownTimer?.cancel()
    }
}

