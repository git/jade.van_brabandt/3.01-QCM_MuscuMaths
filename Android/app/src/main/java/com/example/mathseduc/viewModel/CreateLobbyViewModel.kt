package com.example.mathseduc.viewModel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.unit.IntSize
import androidx.lifecycle.ViewModel
import com.example.mathseduc.MainActivity
import com.example.mathseduc.controllers.ControllerChapter
import com.example.mathseduc.controllers.ControllerLobby
import com.example.mathseduc.controllers.ControllerPlayer
import com.example.mathseduc.models.Lobby
import com.example.mathseduc.models.Player
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import okhttp3.MultipartBody

class CreateLobbyViewModel : ViewModel() {

    private var lobbyNameState = MutableStateFlow("")
    val lobbyName : StateFlow<String> = lobbyNameState

    private var passwordState = MutableStateFlow("")
    val password: StateFlow<String> = passwordState

    private var nbPlayersState = MutableStateFlow("")
    val nbPlayers: StateFlow<String> = nbPlayersState

    private var difficultyState = MutableStateFlow("")
    val difficulty: StateFlow<String> = difficultyState

    private var chapterState = MutableStateFlow("")
    val chapter : StateFlow<String> = chapterState

    private var expandedDifficultyState = MutableStateFlow(false)
    val expandedDifficulty: StateFlow<Boolean> = expandedDifficultyState

    private var expandedChapterState = MutableStateFlow(false)
    val expandedChapter: StateFlow<Boolean> = expandedChapterState

    private var sizeState = MutableStateFlow(IntSize.Zero)
    val size : StateFlow<IntSize> = sizeState

    fun updateLobbyName(lobbyName : String) {
        this.lobbyNameState.update { lobbyName }
    }

    fun updatePassword(password : String) {
        this.passwordState.update { password }
    }

    fun updateNbPlayers(nbPlayers : String) {
        this.nbPlayersState.update { nbPlayers }
    }

    fun updateDifficulty(difficulty : String) {
        this.difficultyState.update { difficulty }
    }

    fun updatechapter(chapter : String) {
        this.chapterState.update { chapter }
    }

    fun updateExpandedChapter(expandedChapter : Boolean) {
        this.expandedChapterState.update { expandedChapter }
    }

    fun updateExpandedDifficulty(expandedDifficulty : Boolean) {
        this.expandedDifficultyState.update { expandedDifficulty }
    }

    fun updateSize(size : IntSize) {
        this.sizeState.update { size }
    }
}