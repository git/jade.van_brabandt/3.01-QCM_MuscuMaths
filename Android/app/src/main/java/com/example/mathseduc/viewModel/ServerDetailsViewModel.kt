package com.example.mathseduc.viewModel

import androidx.lifecycle.ViewModel
import com.example.mathseduc.MainActivity
import com.example.mathseduc.controllers.ControllerChapter
import com.example.mathseduc.controllers.ControllerLobby
import com.example.mathseduc.controllers.ControllerPlayer
import com.example.mathseduc.models.Lobby
import com.example.mathseduc.models.Player
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import okhttp3.MultipartBody

class ServerDetailsViewModel : ViewModel() {

    private var playerListState = MutableStateFlow<List<Int>>(emptyList())
    val playerList : StateFlow<List<Int>> = playerListState

    private var playerListInfosState = MutableStateFlow<List<Player>>(emptyList())
    val playerListInfos: StateFlow<List<Player>> = playerListInfosState

    private var isCreatorState = MutableStateFlow(false)
    val isCreator: StateFlow<Boolean> = isCreatorState

    private var showDialogState = MutableStateFlow(false)
    val showDialog: StateFlow<Boolean> = showDialogState

    private var refreshStateState = MutableStateFlow(true)
    val refreshState : StateFlow<Boolean> = refreshStateState

    fun updatePlayerList(lobbyId : Int?) {
        this.playerListState.update {
            ControllerPlayer.getPlayersIdFromLobbyId(lobbyId.toString()) ?: emptyList()
        }
    }

    fun updatePlayerListInfos() {
        this.playerListInfosState.update {
            playerList.value.mapNotNull { playerId -> ControllerPlayer.getPlayerInfoById(playerId.toString()) }
        }
    }

    fun updateIsCreator(lobbyId : Int?) {
        this.isCreatorState.update {
            ControllerLobby.playerCreatorIdPresentInLobby(MainActivity.idPlayerConnected, lobbyId)
        }
    }

    fun updateRefresh(bool : Boolean) {
        this.refreshStateState.update { bool }
    }

    fun updateShowDialog(bool : Boolean) {
        this.showDialogState.update { bool }
    }

    fun Launchedlobby(lobbyId : Int?): Boolean{
        return ControllerLobby.lobbyIsLaunched(lobbyId)
    }

    fun getNbPlayerInLobby(lobbyId : Int?) : Int{
        return ControllerLobby.getNbPlayerInLobby(lobbyId)
    }

    fun getChapterNameById(chapterId: Int?): String?{
        return ControllerChapter.getChapterNameById(chapterId)
    }

    fun updateLobbyLauched(lobbyId : Int?,formDataBuilder: MultipartBody.Builder){
        ControllerLobby.updateLobbyLauched(lobbyId,formDataBuilder)
    }
}