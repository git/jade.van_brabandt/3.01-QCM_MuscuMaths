package com.example.mathseduc.ui

import android.app.Activity
import android.content.Intent
import android.content.res.Configuration
import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalView
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.navigation.NavController
import com.example.mathseduc.ConnexionPlayerActivity
import com.example.mathseduc.MainActivity
import com.example.mathseduc.MultiActivity
import com.example.mathseduc.QuizMultiActivity
import com.example.mathseduc.R
import com.example.mathseduc.ui.theme.Colors


@Composable
fun HomePage(navController: NavController) {
    val context = LocalContext.current
    val modifier = Modifier
        .fillMaxSize()
        .padding(16.dp)

    val view = LocalView.current
    val isPortrait = LocalConfiguration.current.orientation == Configuration.ORIENTATION_PORTRAIT
    val window = (view.context as Activity).window
    WindowCompat.setDecorFitsSystemWindows(window, false)

    val windowInsetsController = remember {
        WindowCompat.getInsetsController(window, window.decorView)
    }

    windowInsetsController.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE

    windowInsetsController.hide(WindowInsetsCompat.Type.systemBars())

    Column(
        modifier = modifier,
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(
            painter = painterResource(id = R.drawable.logo),
            contentDescription = null,
            modifier = Modifier
                .size(160.dp, 130.dp)
                .clip(RoundedCornerShape(8.dp))
        )

        Spacer(modifier = Modifier.height(16.dp))

        Button(
            onClick = {navController.navigate("themeChoice")},
            shape = RoundedCornerShape(15),
            colors = ButtonDefaults.buttonColors(Colors.Green),
            modifier = Modifier
                .fillMaxWidth(if (isPortrait) 1f else 0.5f)
                .height(48.dp)

        ) {
            Text(
                text = "Solo",
                color = Color.White,
                fontWeight = FontWeight.Bold
            )
        }

        Spacer(modifier = Modifier.height(16.dp))

        Button(
            onClick = {
                if (MainActivity.idPlayerConnected != -1){
                    navController.navigate("multiplayer")
                }
                else {
                    Toast.makeText(context, "Vous n'êtes pas connecté", Toast.LENGTH_SHORT).show()
                    navController.navigate("connexion")
                }
            },
            shape = RoundedCornerShape(15),
            colors = ButtonDefaults.buttonColors(Colors.Orange),
            modifier = Modifier
                .fillMaxWidth(if (isPortrait) 1f else 0.5f)
                .height(48.dp)
        ) {
            Text(
                text = "Multiplayer",
                color = Color.White,
                fontWeight = FontWeight.Bold
            )
        }

        Spacer(modifier = Modifier.height(16.dp))

        if(MainActivity.idPlayerConnected != -1){
            Button(
                    onClick = {
                        navController.navigate("profilePlayer")
                    },
                    shape = RoundedCornerShape(15),
                    colors = ButtonDefaults.buttonColors(Colors.Grey),
                    modifier = Modifier
                            .fillMaxWidth(if (isPortrait) 1f else 0.5f)
                            .height(48.dp)
                            .padding(horizontal = 16.dp)
            ) {
                Text(
                        text = "Profile",
                        color = Color.White
                )
            }
        }
        else {
            Button(
                    onClick = {
                        navController.navigate("connexion")
                    },
                    shape = RoundedCornerShape(15),
                    colors = ButtonDefaults.buttonColors(Colors.Grey),
                    modifier = Modifier
                            .fillMaxWidth(if (isPortrait) 1f else 0.5f)
                            .height(48.dp)
                            .padding(horizontal = 16.dp)
            ) {
                Text(
                        text = "Connexion",
                        color = Color.White
                )
            }
        }

    }
}