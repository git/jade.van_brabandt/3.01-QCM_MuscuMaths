package com.example.mathseduc.models

import Answer

data class Question(
    val id: Int,
    val content: String,
    val nbfails: Int,
    val difficulty: Int,
    val idchapter: Int,
    val idanswergood: Int,
    val answers: List<Answer>
)