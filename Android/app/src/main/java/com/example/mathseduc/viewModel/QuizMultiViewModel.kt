package com.example.mathseduc.viewModel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.lifecycle.ViewModel
import com.example.mathseduc.controllers.ControllerLobby
import com.example.mathseduc.controllers.ControllerQuestion
import com.example.mathseduc.models.Lobby
import com.example.mathseduc.models.Player
import com.example.mathseduc.models.Question
import com.example.mathseduc.models.Utiliser
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.count
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.flow.withIndex
import kotlin.coroutines.CoroutineContext

class QuizMultiViewModel : ViewModel() {

    private var chronoValueState = MutableStateFlow(0.0f)
    val chronoValue : StateFlow<Float> = chronoValueState

    private var listQuestionState = MutableStateFlow<List<Question>?>(emptyList())
    val listQuestion : StateFlow<List<Question>?> = listQuestionState

    private var listPlayerState = MutableStateFlow<List<Utiliser>>(emptyList())
    val listPlayer : StateFlow<List<Utiliser>> = listPlayerState

    private var currentQuestionIndexState = MutableStateFlow(0)
    val currentQuestionIndex : StateFlow<Int> = currentQuestionIndexState

    private var progressBarValuesState = MutableStateFlow<List<Float>>(emptyList())
    val progressBarValues : StateFlow<List<Float>> = progressBarValuesState

    private var progressBarTotalValuesState = MutableStateFlow<List<Float>>(emptyList())
    val progressBarTotalValues : StateFlow<List<Float>> = progressBarTotalValuesState

    private var quizFinishedState = MutableStateFlow(false)
    val quizFinished : StateFlow<Boolean> = quizFinishedState

    private var windowInsetsControllerState = MutableStateFlow(null)
    val windowInsetsController : StateFlow<WindowInsetsControllerCompat?> = windowInsetsControllerState

    fun updateListQuestion(lobbyId : Int?){
        this.listQuestionState.update{ ControllerQuestion.getQuestionsForLobby(ControllerLobby.getIdQuestionsLobby(lobbyId!!)) }
    }
    fun updateListPlayer(lobbyId : Int?){
        this.listPlayerState.update{ ControllerLobby.getPlayerInLobby(lobbyId!!) }
    }
    fun initializeProgressBar(){
        this.progressBarValuesState.update{ List(this.listPlayer.value.size) { 0.0f } }
    }
    fun initializeProgressBarTotal(){
        this.progressBarTotalValuesState.update{ List(this.listPlayer.value.size) { 0.0f } }
    }
    fun updateChronoValue(time : Float){
        this.chronoValueState.update { this.chronoValue.value + time }
    }
    fun resetChronoValue(){
        this.chronoValueState.update { 0.0f }
    }
    fun updateCurrentQuestionIndex(){
        this.currentQuestionIndexState.update { this.currentQuestionIndex.value + 1 }
    }

    fun updateQuizFinished(bool : Boolean){
        this.quizFinishedState.update { bool }
    }

    fun updateProgressBarValues(index : Int){
        this.progressBarValuesState.update {
            val newList = it.toMutableList()
            newList[index] += 1f

            newList
        }
    }

    fun updateProgressBarTotalValues(index : Int,valueBD : List<Utiliser>){
        this.progressBarTotalValuesState.update {
            val newList = it.toMutableList()
            newList[index] += this.progressBarValues.value[index] + valueBD[index].playertime.toFloat()

            newList
        }
    }
}