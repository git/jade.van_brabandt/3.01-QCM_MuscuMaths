package com.example.mathseduc.controllers

import android.os.StrictMode
import android.util.Log
import com.example.mathseduc.models.Lobby
import com.example.mathseduc.models.Utiliser
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import java.io.IOException

class ControllerUtiliser {
    companion object {

        fun createUtiliserByIdLobby(utiliserData: MultipartBody.Builder): Boolean {
            try {
                val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
                StrictMode.setThreadPolicy(policy)

                // Client HTTP API
                val client = OkHttpClient()

                // API Access
                val request = Request.Builder()
                    .url("https://trusting-panini.87-106-126-109.plesk.page/api/add/utiliser/qUOGkWdoPCgbmuqxIC8xiaX0rV1Pw1LoPafkaoHOgszEyD9P2vcOu493xCDZpAqO")
                    .post(utiliserData.build())
                    .build()

                // API Response
                client.newCall(request).execute().use { response ->
                    // Vérifier si la création du lobby a réussi
                    return response.isSuccessful
                }
            } catch (e: Exception) {
                // Log en cas d'erreur
                Log.e("CreateLobby", "Error creating lobby", e)
                return false
            }
        }

        fun DeleteUtiliserForLobby(idPlayerConnected: Int, lobbyId: Int): Boolean {
            try {
                val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
                StrictMode.setThreadPolicy(policy)

                // Client HTTP API
                val client = OkHttpClient()

                // API Access
                val request = Request.Builder()
                    .url("https://trusting-panini.87-106-126-109.plesk.page/api/delete/utiliser/$lobbyId/$idPlayerConnected/qUOGkWdoPCgbmuqxIC8xiaX0rV1Pw1LoPafkaoHOgszEyD9P2vcOu493xCDZpAqO")
                    .delete()
                    .build()

                // API Response
                val response: Response = client.newCall(request).execute()

                // Vérifier si la suppression a réussi
                return response.isSuccessful
            } catch (e: Exception) {
                // Log en cas d'erreur
                Log.e("DeleteUtiliser", "Error deleting utiliser", e)
                return false
            }
        }
        fun getIdNextPlayerInLobby(lobbyId: Int): Int {

            val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
            StrictMode.setThreadPolicy(policy)

            // Client HTTP API
            val client = OkHttpClient()

            // API Access
            val request = Request.Builder()
                .url("https://trusting-panini.87-106-126-109.plesk.page/api/utiliser/$lobbyId/qUOGkWdoPCgbmuqxIC8xiaX0rV1Pw1LoPafkaoHOgszEyD9P2vcOu493xCDZpAqO")
                .build()

            // API Response
            client.newCall(request).execute().use { response ->
                if (!response.isSuccessful) throw IOException("Unexpected code $response")

                // Gson deserialization
                val gson = Gson()
                val typeTokenProduct = object : TypeToken<ArrayList<Utiliser>>() {}.type

                val utiliser: ArrayList<Utiliser> = gson.fromJson(response.body!!.string(), typeTokenProduct)
                if (utiliser.isNotEmpty()){
                    return utiliser[0].idplayer
                } else {
                    return -1
                }
            }
            return -1
        }
    }
}
