package com.example.mathseduc.controllers

import android.os.StrictMode
import android.util.Log
import com.example.mathseduc.models.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.FormBody
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.mindrot.jbcrypt.BCrypt
import java.io.IOException

class ControllerPlayer {
    companion object {

        fun getPlayersIdFromLobbyId(lobbyId: String?): List<Int>? {
            val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
            StrictMode.setThreadPolicy(policy)

            val client = OkHttpClient()

            val request = Request.Builder()
                .url("https://trusting-panini.87-106-126-109.plesk.page/api/utiliser/$lobbyId/qUOGkWdoPCgbmuqxIC8xiaX0rV1Pw1LoPafkaoHOgszEyD9P2vcOu493xCDZpAqO")
                .build()

            client.newCall(request).execute().use { response ->
                if (!response.isSuccessful) throw IOException("Unexpected code $response")

                val gson = Gson()
                val typeTokenProduct = object : TypeToken<List<Utiliser>>() {}.type

                val playerInfoList: List<Utiliser> = gson.fromJson(response.body!!.string(), typeTokenProduct)

                // Extract player IDs from PlayerInfo list
                val playerIds: List<Int> = playerInfoList.map { it.idplayer }

                return playerIds
            }

            return null
        }

        fun getPlayerInfoById(playerId: String): Player? {
            val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
            StrictMode.setThreadPolicy(policy)

            val client = OkHttpClient()

            val request = Request.Builder()
                .url("https://trusting-panini.87-106-126-109.plesk.page/api/players/$playerId/qUOGkWdoPCgbmuqxIC8xiaX0rV1Pw1LoPafkaoHOgszEyD9P2vcOu493xCDZpAqO")
                .build()

            client.newCall(request).execute().use { response ->
                if (!response.isSuccessful) throw IOException("Unexpected code $response")

                val gson = Gson()
                val playerInfo: List<Player> = gson.fromJson(response.body!!.string(), object : TypeToken<List<Player>>() {}.type)

                // Assuming the API returns a list even for a single player, we take the first item
                return playerInfo.firstOrNull()
            }

            return null
        }

        fun createPlayer(nickname: String, password: String): Int {
            val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
            StrictMode.setThreadPolicy(policy)

            val client = OkHttpClient()

            val hashedPassword = BCrypt.hashpw(password, BCrypt.gensalt())

            val requestBody = FormBody.Builder()
                    .add("nickname", nickname)
                    .add("password", hashedPassword)
                    .build()

            val request = Request.Builder()
                    .url("https://trusting-panini.87-106-126-109.plesk.page/api/add/player/qUOGkWdoPCgbmuqxIC8xiaX0rV1Pw1LoPafkaoHOgszEyD9P2vcOu493xCDZpAqO")
                    .post(requestBody)
                    .build()

            client.newCall(request).execute().use { response ->
                if (response.isSuccessful) {
                    val responseData = response.body?.string()

                    val gson = Gson()
                    val player: Player = gson.fromJson(responseData, Player::class.java)

                    return player.id
                } else {
                    throw IOException("Failed to create player: ${response.code}")
                }
            }
        }

        fun deletePlayer(playerId: String) {
            try {
                val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
                StrictMode.setThreadPolicy(policy)

                // Client HTTP API
                val client = OkHttpClient()

                // API Access - Suppression du player
                val deleteRequest = Request.Builder()
                        .url("https://trusting-panini.87-106-126-109.plesk.page/api/delete/player/$playerId/qUOGkWdoPCgbmuqxIC8xiaX0rV1Pw1LoPafkaoHOgszEyD9P2vcOu493xCDZpAqO")
                        .delete()
                        .build()

                // API Response
                val deleteResponse: Response = client.newCall(deleteRequest).execute()

                // Vérifier si la suppression a réussi
                if (!deleteResponse.isSuccessful) {
                    Log.e("deletePlayer", "Error deleting player")
                }
            } catch (e: Exception) {
                // Log en cas d'erreur
                Log.e("deletePlayer", "Error deleting player", e)
            }
        }

        fun updatePlayer(playerId: String,lobbyData: MultipartBody.Builder) {
            try {
                val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
                StrictMode.setThreadPolicy(policy)

                val client = OkHttpClient()

                val updateRequest = Request.Builder()
                        .url("https://trusting-panini.87-106-126-109.plesk.page/api/update/player/${playerId}/qUOGkWdoPCgbmuqxIC8xiaX0rV1Pw1LoPafkaoHOgszEyD9P2vcOu493xCDZpAqO")
                        .post(lobbyData.build())
                        .build()

                val updateResponse: Response = client.newCall(updateRequest).execute()

                if (!updateResponse.isSuccessful) {
                        Log.e("updatePlayer", "Error updating player")
                }
            } catch (e: Exception) {
                    Log.e("updatePlayer", "Error updating lobby", e)
            }
        }

        fun authenticateUser(nickname: String, password: String): Int {

            val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
            StrictMode.setThreadPolicy(policy)

            // Client HTTP API
            val client = OkHttpClient()

            // API Access
            val request = Request.Builder()
                .url("https://trusting-panini.87-106-126-109.plesk.page/api/verif/player/$nickname/qUOGkWdoPCgbmuqxIC8xiaX0rV1Pw1LoPafkaoHOgszEyD9P2vcOu493xCDZpAqO")
                .build()

            // API Response
            client.newCall(request).execute().use { response ->
                if (response.isSuccessful) {
                    val responseData = response.body?.string()

                    // Use Gson to parse the JSON response
                    val gson = Gson()
                    val playerListType = object : TypeToken<List<Player>>() {}.type
                    val playerList: List<Player> = gson.fromJson(responseData, playerListType)

                    // Check if the list is not empty
                    if (playerList.isNotEmpty()) {
                        val storedPasswordHash = playerList[0].password

                        // Use BCrypt to check if the provided password matches the stored hash
                        if(BCrypt.checkpw(password, storedPasswordHash)){
                            return playerList[0].id
                        }
                    }
                }
            }
            return -1
        }
    }
}