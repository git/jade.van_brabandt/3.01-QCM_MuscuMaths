package com.example.mathseduc

import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.mathseduc.ui.CreateLobbyPage
import com.example.mathseduc.ui.HomePage
import com.example.mathseduc.ui.MultiPage
import com.example.mathseduc.ui.QuizMultiScreen
import com.example.mathseduc.ui.ServerDetailPage
import com.example.mathseduc.ui.ProfilePlayerPage
import com.example.mathseduc.ui.ThemeChoicePage

@Composable
fun AppNavigation() {
    val navController = rememberNavController()

    NavHost(navController = navController, startDestination = "home") {
        composable("home") { HomePage(navController) }
        composable("connexion") { ConnexionPlayerContent(navController) }
        composable("multiplayer") { MultiPage(navController) }
        composable("createLobby") { CreateLobbyPage(navController) }
        composable("profilePlayer") { ProfilePlayerPage(navController) }
        composable("themeChoice") { ThemeChoicePage(navController) }
        //composable("serverDetails/{serverName}/{lobbyId}") { ServerDetailPage(navController) }
        composable(
            route = "serverDetails/{lobbyName}/{lobbyId}/{lobbyChapter}/{lobbyNbPlayers}/{lobbyDifficulty}",
            arguments = listOf(
                navArgument("lobbyName") { type = NavType.StringType },
                navArgument("lobbyId") { type = NavType.IntType },
                navArgument("lobbyChapter") { type = NavType.IntType },
                navArgument("lobbyNbPlayers") { type = NavType.IntType },
                navArgument("lobbyDifficulty") { type = NavType.IntType }
            )
        ) { backStackEntry ->
            val lobbyName = backStackEntry.arguments?.getString("lobbyName")
            val lobbyId = backStackEntry.arguments?.getInt("lobbyId")
            val lobbyChapter = backStackEntry.arguments?.getInt("lobbyChapter")
            val lobbyNbPlayers = backStackEntry.arguments?.getInt("lobbyNbPlayers")
            val lobbyDifficulty = backStackEntry.arguments?.getInt("lobbyDifficulty")

            ServerDetailPage(navController, lobbyName,lobbyId, lobbyChapter, lobbyNbPlayers, lobbyDifficulty)
        }
        composable(
            route = "quizMultiScreen/{lobbyId}",
            arguments = listOf(
                navArgument("lobbyId") { type = NavType.IntType }
            )
        ) { backStackEntry ->
            val lobbyId = backStackEntry.arguments?.getInt("lobbyId")
            QuizMultiScreen(navController, lobbyId)
        }
    }
}