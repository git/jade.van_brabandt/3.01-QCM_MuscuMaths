package com.example.mathseduc.ui

import android.app.Activity
import android.content.res.Configuration
import androidx.compose.foundation.border
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalView
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.navigation.NavController
import com.example.mathseduc.controllers.ControllerChapter
import com.example.mathseduc.ui.theme.Colors

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ThemeChoicePage(navController: NavController){
    var selectedDifficulty by rememberSaveable { mutableStateOf("") }
    var selectedChapter by rememberSaveable { mutableStateOf("") }

    val isPortrait = LocalConfiguration.current.orientation == Configuration.ORIENTATION_PORTRAIT
    val activity = LocalView.current.context as Activity
    val chapterOptions = ControllerChapter.getChapters()

    val windowInsetsController = remember {
        WindowCompat.getInsetsController(activity.window, activity.window.decorView)
    }

    windowInsetsController.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE

    windowInsetsController.hide(WindowInsetsCompat.Type.systemBars())

    TopAppBar(
            colors = TopAppBarDefaults.topAppBarColors(
                    containerColor = Color.Transparent,
            ),
            title = {},
            navigationIcon = {
                IconButton(
                        onClick = { navController.navigate("home") },
                        modifier = Modifier.size(60.dp)
                ) {
                    Icon(
                            imageVector = Icons.Filled.ArrowBack,
                            contentDescription = "Retour",
                            modifier = Modifier.size(36.dp),
                            tint = Color.White
                    )
                }
            },
    )
    Column(
            modifier = Modifier
                    .fillMaxSize()
                    .padding(14.dp),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
                text = "Difficulty :",
                fontSize = 25.sp,
                color = Color.White,
        )

        Spacer(modifier = Modifier.height(15.dp))

        Row {
            Button(
                    onClick = { selectedDifficulty = "1"},
                    shape = RoundedCornerShape(20),
                    colors = ButtonDefaults.buttonColors(if (selectedDifficulty == "1") Colors.Green else Color.Transparent),
                    modifier = Modifier
                            .height(60.dp)
                            .border(
                                    width = 1.dp,
                                    color = Colors.Green,
                                    shape = RoundedCornerShape(20)
                            )
            ) {
                Text(
                        text = "Easy",
                        color = Color.White,
                        fontSize  = 20.sp
                )
            }
            Spacer(modifier = Modifier.width(if(isPortrait) 15.dp else 20.dp))
            Button(
                    onClick = { selectedDifficulty = "2"},
                    shape = RoundedCornerShape(20),
                    colors = ButtonDefaults.buttonColors(if (selectedDifficulty == "2") Colors.Orange else Color.Transparent),
                    modifier = Modifier
                            .height(60.dp)
                            .border(
                                    width = 1.dp,
                                    color = Colors.Orange,
                                    shape = RoundedCornerShape(20)

                            )
            ) {
                Text(
                        text = "Medium",
                        color = Color.White,
                        fontSize = 20.sp
                )
            }
            Spacer(modifier = Modifier.width(if(isPortrait) 15.dp else 20.dp))
            Button(
                    onClick = { selectedDifficulty = "3"},
                    shape = RoundedCornerShape(20),
                    colors = ButtonDefaults.buttonColors(if (selectedDifficulty == "3") Colors.Red else Color.Transparent),
                    modifier = Modifier
                            .height(60.dp)
                            .border(
                                    width = 1.dp,
                                    color = Colors.Red,
                                    shape = RoundedCornerShape(20)
                            )
            ) {
                Text(
                        text = "Hard",
                        color = Color.White,
                        fontSize  = 20.sp
                )
            }
        }

        Spacer(modifier = Modifier.height(if(isPortrait) 25.dp else 30.dp))

        Text(
                text = "Chapter :",
                fontSize = 25.sp,
                color = Color.White,
        )

        Row(
                horizontalArrangement = Arrangement.spacedBy(15.dp),
                modifier = Modifier
                        .padding(vertical = 8.dp)
                        .horizontalScroll(rememberScrollState())
        ) {
            if (chapterOptions != null) {
                chapterOptions.forEach { chapter ->
                    Button(
                            onClick = { selectedChapter = chapter.name },
                            shape = RoundedCornerShape(10),
                            modifier = Modifier
                                    .padding(vertical = 5.dp)
                                    .border(
                                            width = 1.dp,
                                            color = Color.Gray,
                                            shape = RoundedCornerShape(10)
                                    ),
                            colors = ButtonDefaults.buttonColors(if (selectedChapter == chapter.name) Color.Gray else Color.Transparent)
                    ) {
                        Text(text = chapter.name, modifier = Modifier.padding(horizontal = 1.dp, vertical = 8.dp))
                    }
                }
            }
        }

        Spacer(modifier = Modifier.height(if(isPortrait) 15.dp else 20.dp))

        Button(
                onClick = {},
                shape = RoundedCornerShape(15),
                colors = ButtonDefaults.buttonColors(Colors.Green),
                modifier = Modifier
                        .fillMaxWidth(if (isPortrait) 1f else 0.5f)
                        .height(48.dp)

        ) {
            Text(
                    text = "Start Game",
                    color = Color.White,
                    fontWeight = FontWeight.Bold
            )
        }
    }
}