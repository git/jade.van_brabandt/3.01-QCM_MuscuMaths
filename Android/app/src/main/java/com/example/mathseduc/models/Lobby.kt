package com.example.mathseduc.models

import android.os.Parcel
import android.os.Parcelable

data class Lobby(
    val id: Int,
    val name: String,
    val password: String,
    val nbplayers: Int,
    val idplayercreator: Int,
    val idchapter: Int,
    val difficulty: Int,
    val launched: Int
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(name)
        parcel.writeString(password)
        parcel.writeInt(nbplayers)
        parcel.writeInt(idplayercreator)
        parcel.writeInt(idchapter)
        parcel.writeInt(difficulty)
        parcel.writeInt(launched)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Lobby> {
        override fun createFromParcel(parcel: Parcel): Lobby {
            return Lobby(parcel)
        }

        override fun newArray(size: Int): Array<Lobby?> {
            return arrayOfNulls(size)
        }
    }
}