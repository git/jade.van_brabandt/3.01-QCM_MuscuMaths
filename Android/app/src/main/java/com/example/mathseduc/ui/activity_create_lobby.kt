package com.example.mathseduc.ui


import android.app.Activity
import android.content.res.Configuration
import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material3.Button
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.platform.LocalView
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import androidx.navigation.NavController
import com.example.mathseduc.MainActivity
import com.example.mathseduc.controllers.ControllerChapter
import com.example.mathseduc.controllers.ControllerLobby
import com.example.mathseduc.controllers.ControllerUtiliser
import com.example.mathseduc.ui.theme.Colors
import com.example.mathseduc.viewModel.CreateLobbyViewModel
import com.example.mathseduc.viewModel.ServerDetailsViewModel
import okhttp3.MultipartBody

@Composable
fun CreateLobbyPage(navController: NavController) {
    val viewModelStoreOwner = LocalContext.current as ViewModelStoreOwner

    val viewModel = ViewModelProvider(viewModelStoreOwner).get(CreateLobbyViewModel::class.java)

    val difficultyOptions = listOf("Facile", "Moyen", "Difficile")

    val lobbyName by viewModel.lobbyName.collectAsState("")
    val password by viewModel.password.collectAsState("")
    val nbPlayers by viewModel.nbPlayers.collectAsState("")
    val difficulty by viewModel.difficulty.collectAsState("")
    val chapter by viewModel.chapter.collectAsState("")
    val expandedDifficulty by viewModel.expandedDifficulty.collectAsState(false)
    val expandedChapter by viewModel.expandedChapter.collectAsState(false)
    val size by viewModel.size.collectAsState(IntSize.Zero)

    val context = LocalContext.current
    val activity = LocalView.current.context as Activity
    val keyboardController = LocalSoftwareKeyboardController.current
    val isPortrait = LocalConfiguration.current.orientation == Configuration.ORIENTATION_PORTRAIT

    val windowInsetsController = remember {
        WindowCompat.getInsetsController(activity.window, activity.window.decorView)
    }

    windowInsetsController.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE

    windowInsetsController.hide(WindowInsetsCompat.Type.systemBars())

    Column(
        modifier = Modifier
                .fillMaxSize()
                .padding(if (isPortrait) 16.dp else 5.dp),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
                text = "Create Lobby",
                fontSize = (if(isPortrait) 30.sp else 0.sp),
                color = Color.White,
                )

        Spacer(modifier = Modifier.height(if(isPortrait)25.dp else 0.dp))

        Column(
                modifier = Modifier.background(Colors.White, shape = RoundedCornerShape(8.dp)),
                horizontalAlignment = Alignment.CenterHorizontally
        ) {
            TextField(
                    value = lobbyName,
                    onValueChange = { viewModel.updateLobbyName(it) },
                    modifier = Modifier
                            .fillMaxWidth(if (isPortrait) 1f else 0.5f)
                            .padding(if (isPortrait) 8.dp else 4.dp),
                    shape = RoundedCornerShape(8.dp),
                    label = { Text("Lobby Name") }
            )


            TextField(
                    value = password,
                    onValueChange = { viewModel.updatePassword(it) },
                    modifier = Modifier
                            .fillMaxWidth(if (isPortrait) 1f else 0.5f)
                            .padding(if (isPortrait) 8.dp else 4.dp),
                    shape = RoundedCornerShape(8.dp),
                    label = { Text("Password") },
                    visualTransformation = PasswordVisualTransformation(),
                    keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Password)
            )


            TextField(
                    value = nbPlayers,
                    onValueChange = { viewModel.updateNbPlayers(it) },
                    modifier = Modifier
                            .fillMaxWidth(if (isPortrait) 1f else 0.5f)
                            .padding(if (isPortrait) 8.dp else 4.dp),
                    label = { Text("Number of Players") },
                    keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Number)
            )


            Box{
                TextField(
                        value = difficulty,
                        onValueChange = { viewModel.updateDifficulty(it) },
                        readOnly = true,
                        enabled = false,
                        colors = TextFieldDefaults.colors(
                                disabledTextColor = Colors.Black,
                                disabledLabelColor = Colors.Black,
                                disabledTrailingIconColor = Colors.Black,
                                disabledIndicatorColor = Color.Black
                        ),
                        modifier = Modifier
                                .clickable(onClick = {
                                    viewModel.updateExpandedDifficulty(!expandedDifficulty)
                                })
                                .onGloballyPositioned {
                                    viewModel.updateSize(it.size)
                                }
                                .fillMaxWidth(if (isPortrait) 1f else 0.5f)
                                .padding(if (isPortrait) 8.dp else 4.dp),
                        label = { Text("Difficulté") },
                        trailingIcon = {
                            Icon(
                                    Icons.Default.ArrowDropDown,
                                    contentDescription = null,
                                    Modifier.clickable {
                                        viewModel.updateExpandedDifficulty(!expandedDifficulty)
                                    }
                            )
                        }
                )
                DropdownMenu(
                        expanded = expandedDifficulty,
                        onDismissRequest = { viewModel.updateExpandedDifficulty(false) },
                        modifier = Modifier
                                .width(with(LocalDensity.current) { size.width.toDp() })
                                .padding(if (isPortrait) 8.dp else 4.dp)
                ) {
                    difficultyOptions.forEach { option ->
                        DropdownMenuItem(
                                text = { Text(option, color = Colors.Black) },
                                onClick = {
                                    viewModel.updateDifficulty(option)
                                    viewModel.updateExpandedDifficulty(false)
                                    keyboardController?.hide()
                                }
                        )
                    }
                }
            }


            Box{
                val chapterOptions = ControllerChapter.getChapters()
                TextField(
                        value = chapter,
                        onValueChange = { viewModel.updatechapter(it) },
                        readOnly = true,
                        enabled = false,
                        colors = TextFieldDefaults.colors(
                                disabledTextColor = Colors.Black,
                                disabledLabelColor = Colors.Black,
                                disabledTrailingIconColor = Colors.Black,
                                disabledIndicatorColor = Color.Black
                        ),
                        modifier = Modifier
                                .clickable(onClick = {
                                    viewModel.updateExpandedChapter(!expandedChapter)
                                })
                                .onGloballyPositioned {
                                    viewModel.updateSize(it.size)
                                }
                                .fillMaxWidth(if (isPortrait) 1f else 0.5f)
                                .padding(if (isPortrait) 8.dp else 4.dp),
                        label = { Text("Chapitre") },
                        trailingIcon = {
                            Icon(
                                    Icons.Default.ArrowDropDown,
                                    contentDescription = null,
                                    Modifier.clickable {
                                        viewModel.updateExpandedChapter(!expandedChapter)
                                    }
                            )
                        }
                )
                DropdownMenu(
                        expanded = expandedChapter,
                        onDismissRequest = { viewModel.updateExpandedChapter(false) },
                        modifier = Modifier
                                .width(with(LocalDensity.current) { size.width.toDp() })
                                .padding(if (isPortrait) 8.dp else 4.dp)
                ) {

                    chapterOptions?.forEach { option ->
                        DropdownMenuItem(
                                text = { Text(option.name, color = Colors.Black) },
                                onClick = {
                                    viewModel.updatechapter( option.name )
                                    viewModel.updateExpandedChapter(false)
                                    keyboardController?.hide()
                                }
                        )
                    }
                }
            }
        }



        Button(
            onClick = {
                // Handle button click (Create Lobby)
                val selectedChapter = ControllerChapter.getChapters()?.find { it.name == chapter }
                val difficultyNum = difficultyOptions.indexOf(difficulty) + 1

                if (lobbyName.isBlank() || nbPlayers.isBlank() || nbPlayers.toInt() <= 0 || difficulty.isBlank() || selectedChapter == null) {
                    // Show error message if any field is invalid
                    Toast.makeText(context, "Invalid input. Please check all fields.", Toast.LENGTH_SHORT).show()
                } else {
                    try {
                        val formDataBuilder = MultipartBody.Builder().setType(MultipartBody.FORM)

                        // Add fields to form data
                        formDataBuilder.addFormDataPart("name", lobbyName)
                        formDataBuilder.addFormDataPart("password", password)
                        formDataBuilder.addFormDataPart("nbplayers", nbPlayers)
                        formDataBuilder.addFormDataPart("idplayercreator", MainActivity.idPlayerConnected.toString())
                        formDataBuilder.addFormDataPart("idchapter", selectedChapter.id)
                        formDataBuilder.addFormDataPart("difficulty", difficultyNum.toString())
                        Log.e("formData : ",formDataBuilder.toString())
                        // Call createLobby function from ControllerLobby
                        val lobbyId = ControllerLobby.createLobby(formDataBuilder)

                        // Check if lobby creation is successful
                        if (lobbyId != -1) {
                            val formDataBuilderConnexion = MultipartBody.Builder().setType(MultipartBody.FORM)
                            formDataBuilderConnexion.addFormDataPart("idplayer", MainActivity.idPlayerConnected.toString())
                            formDataBuilderConnexion.addFormDataPart("idlobby", lobbyId.toString())
                            formDataBuilderConnexion.addFormDataPart("playertime", "0")

                            ControllerUtiliser.createUtiliserByIdLobby(formDataBuilderConnexion)

                            Toast.makeText(context, "Lobby created successfully!", Toast.LENGTH_SHORT).show()
                            navController.navigate("serverDetails/${lobbyName}/${lobbyId}/${selectedChapter.id.toInt()}/${nbPlayers}/${difficultyNum}")
                        } else {
                            Toast.makeText(context, "Failed to create lobby. Please try again.", Toast.LENGTH_SHORT).show()
                        }
                    } catch (e: Exception) {
                        // Log en cas d'erreur
                        Log.e("CreateLobby", "Error creating lobby", e)
                        Toast.makeText(context, "Failed to create lobby. Please try again.", Toast.LENGTH_SHORT).show()
                    }
                }
            },
            modifier = Modifier
                    .fillMaxWidth(if (isPortrait) 1f else 0.5f)
                    .padding(if (isPortrait) 8.dp else 4.dp)
        ) {
            Text(text = "Create Lobby", color = Colors.White)
        }
    }
}



