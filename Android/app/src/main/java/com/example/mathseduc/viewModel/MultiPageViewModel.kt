package com.example.mathseduc.viewModel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import com.example.mathseduc.controllers.ControllerLobby
import com.example.mathseduc.controllers.ControllerPlayer
import com.example.mathseduc.models.Lobby
import com.example.mathseduc.models.Player
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
class MultiPageViewModel : ViewModel() {

    private var lobbyListState = MutableStateFlow<List<Lobby>>(emptyList())
    val lobbyList : StateFlow<List<Lobby>> = lobbyListState

    fun updateLobbyList() {
        this.lobbyListState.update {
            ControllerLobby.getLobbies() ?: emptyList()
        }
    }

    fun getNbPlayerInLobby(selectedItem : Lobby) : Int {
        return ControllerLobby.getNbPlayerInLobby(selectedItem!!.id)
    }
}