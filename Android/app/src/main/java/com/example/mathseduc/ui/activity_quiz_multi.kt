package com.example.mathseduc.ui

import android.app.Activity
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalView
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import com.example.mathseduc.MainActivity
import com.example.mathseduc.controllers.ControllerLobby
import com.example.mathseduc.controllers.ControllerQuestion
import com.example.mathseduc.ui.theme.Colors
import com.example.mathseduc.viewModel.QuizMultiViewModel
import kotlinx.coroutines.channels.ticker
import okhttp3.MultipartBody

@Composable
fun QuizMultiScreen(navController: NavController, lobbyId: Int?) {
    val context = LocalContext.current
    val activity = LocalView.current.context as Activity

    val viewModel = ViewModelProvider(navController.getViewModelStoreOwner(navController.graph.id)).get(QuizMultiViewModel::class.java)

    val chronoValue by viewModel.chronoValue.collectAsState(0.0f)
    val listQuestion by viewModel.listQuestion.collectAsState(ControllerQuestion.getQuestionsForLobby(ControllerLobby.getIdQuestionsLobby(lobbyId!!)))
    val listPlayer by viewModel.listPlayer.collectAsState(ControllerLobby.getPlayerInLobby(lobbyId!!))
    val currentQuestionIndex by viewModel.currentQuestionIndex.collectAsState(0)
    val progressBarValues by viewModel.progressBarValues.collectAsState(List(listPlayer.size) { 0.0f })
    val progressBarTotalValues by viewModel.progressBarTotalValues.collectAsState(List(listPlayer.size) { 0.0f })
    val quizFinished by viewModel.quizFinished.collectAsState(false)
    val windowInsetsController by viewModel.windowInsetsController.collectAsState(WindowCompat.getInsetsController(activity.window, activity.window.decorView))

    windowInsetsController?.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE

    windowInsetsController?.hide(WindowInsetsCompat.Type.systemBars())

    viewModel.updateListQuestion(lobbyId)
    viewModel.updateListPlayer(lobbyId)
    viewModel.initializeProgressBar()
    viewModel.initializeProgressBarTotal()


    LaunchedEffect(Unit) {
        val timer = ticker(delayMillis = 1000) // Update every 1000 milliseconds
        for (tick in timer) {
            if (quizFinished) break
            for ((index, player) in listPlayer.withIndex()) {
                viewModel.updateProgressBarValues(index)
            }
            viewModel.updateChronoValue(1f)
            if (chronoValue >= 30f) {
                viewModel.updateCurrentQuestionIndex()
                viewModel.resetChronoValue()
            }
        }
    }

    LaunchedEffect(Unit) {
        val timer = ticker(delayMillis = 3000)
        for (tick in timer) {
            if (quizFinished) break
            var valueBD = ControllerLobby.getPlayerInLobby(lobbyId!!)
            for ((index, player) in listPlayer.withIndex()) {
                viewModel.updateProgressBarTotalValues(index,valueBD)
            }
        }
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        verticalArrangement = Arrangement.SpaceBetween
    ) {

        for ((index, player) in listPlayer.withIndex()) {
            CustomProgressBar(progressBarTotalValues.toMutableList()[index])
        }

        // Chrono ProgressBar
        ChronoProgressBar(chronoValue)

        if (currentQuestionIndex < listQuestion!!.size) {
            val currentQuestion = listQuestion!![currentQuestionIndex]

            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(8.dp),
                verticalArrangement = Arrangement.SpaceBetween
            ) {
                Row(
                    modifier = Modifier
                        .weight(2f)
                        .padding(vertical = 20.dp)
                ) {
                    Spacer(modifier = Modifier.weight(1f))

                    Box(
                        modifier = Modifier
                            .weight(5f)
                            .background(color = Colors.Grey, shape = RoundedCornerShape(8.dp))
                    ) {
                        Text(
                            text = currentQuestion.content,
                            modifier = Modifier.padding(16.dp),
                            color = Colors.White,
                            fontSize = 20.sp,
                            textAlign = TextAlign.Center
                        )
                    }

                    Spacer(modifier = Modifier.weight(1f))
                }

                Column(modifier = Modifier.weight(1f)) {
                    currentQuestion.answers.forEachIndexed { index, answer ->
                        val buttonBackgroundColor = when (index) {
                            0 -> Colors.Red
                            1 -> Colors.Blue
                            2 -> Colors.Green
                            else -> Colors.Cyan
                        }

                        Button(
                            onClick = {
                                if (answer.id == currentQuestion.idanswergood) {
                                    Toast.makeText(context, "Réussi !!", Toast.LENGTH_SHORT).show()

                                    val formDataBuilder = MultipartBody.Builder().setType(MultipartBody.FORM)
                                    val playertime = ControllerLobby.getLobbyUtiliserPlayerTime(lobbyId!!,MainActivity.idPlayerConnected) + 10
                                    formDataBuilder.addFormDataPart("playertime", playertime.toString())

                                    ControllerLobby.updateLobbyUtiliserPlayerTime(lobbyId,MainActivity.idPlayerConnected,formDataBuilder)
                                } else {
                                    Toast.makeText(context, "Raté !!", Toast.LENGTH_SHORT).show()
                                }
                                viewModel.resetChronoValue()
                                viewModel.updateCurrentQuestionIndex()
                            },
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(bottom = 10.dp),
                            shape = RoundedCornerShape(15),
                            colors = ButtonDefaults.buttonColors(buttonBackgroundColor)
                        ) {
                            Text(text = answer.content)
                        }
                    }
                }
            }

            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1f)
                    .padding(vertical = 10.dp)
            ) {
                Button(
                    onClick = {
                        viewModel.updateCurrentQuestionIndex()
                    },
                    modifier = Modifier
                        .weight(2f),
                    shape = RoundedCornerShape(15),
                    colors = ButtonDefaults.buttonColors(Colors.Grey),
                ) {
                    Text(text = "Passer")
                }
            }
        } else {
            Toast.makeText(context, "Fini !!", Toast.LENGTH_SHORT).show()
            navController.navigate("home")
            viewModel.updateQuizFinished(true)
        }
    }
}

@Composable
fun CustomProgressBar(progressBarValue: Float) {
    LinearProgressIndicator(
        progress = progressBarValue / 100f,
        modifier = Modifier.fillMaxWidth()
    )
}

@Composable
fun ChronoProgressBar(chronoValue: Float) {
    CircularProgressIndicator(
        progress = chronoValue / 30f,
        modifier = Modifier.padding(horizontal = 10.dp)
    )
}
