package com.example.mathseduc.ui

import android.app.Activity
import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalView
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.navigation.NavController
import com.example.mathseduc.MainActivity
import com.example.mathseduc.controllers.ControllerPlayer
import com.example.mathseduc.ui.theme.Colors
import okhttp3.MultipartBody
import org.mindrot.jbcrypt.BCrypt

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ProfilePlayerPage(navController: NavController) {
    val activity = LocalView.current.context as Activity
    val player = ControllerPlayer.getPlayerInfoById(MainActivity.idPlayerConnected.toString())

    var showDialog by rememberSaveable { mutableStateOf(false) }
    var confirmDialog by rememberSaveable { mutableStateOf(false) }

    val isPortrait = LocalConfiguration.current.orientation == Configuration.ORIENTATION_PORTRAIT

    WindowCompat.setDecorFitsSystemWindows(activity.window, false)

    val windowInsetsController = remember {
        WindowCompat.getInsetsController(activity.window, activity.window.decorView)
    }

    windowInsetsController.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE

    // Hide the status bar and navigation bar
    windowInsetsController.hide(WindowInsetsCompat.Type.systemBars())

    TopAppBar(
            colors = TopAppBarDefaults.topAppBarColors(
                    containerColor = Color.Transparent,
            ),
            title = {},
            navigationIcon = {
                IconButton(
                        onClick = { navController.navigate("home") },
                        modifier = Modifier.size(60.dp)
                ) {
                    Icon(
                            imageVector = Icons.Filled.ArrowBack,
                            contentDescription = "Retour",
                            modifier = Modifier.size(36.dp),
                            tint = Color.White
                    )
                }
            },
    )

    Column(
            modifier = Modifier
                    .fillMaxSize()
                    .padding(14.dp),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Column(
                modifier = Modifier
                        .padding(5.dp)
                        .fillMaxWidth(if (isPortrait) 1f else 0.5f)
                        .background(Color.White, RoundedCornerShape(16.dp))
                        .padding(vertical = 8.dp),
                horizontalAlignment = Alignment.CenterHorizontally
        ) {
        Text(
                text = "Profile",
                fontSize = 25.sp,
                color = Color.Black,
                fontWeight = FontWeight.Bold
        )

        Spacer(modifier = Modifier.height(10.dp))

        Divider(
                color = Color.Gray,
                thickness = 2.dp,
                modifier = Modifier.padding(horizontal = 10.dp)
        )

        Spacer(modifier = Modifier.height(10.dp))

        if (player != null) {
            Text(
                    text = "Nickname : ${player.nickname}",
                    fontSize = 19.sp,
                    color = Colors.Black
            )
        }
        Spacer(modifier = Modifier.height(20.dp))

                Button(
                        onClick = { showDialog = true },
                        shape = RoundedCornerShape(15),
                        colors = ButtonDefaults.buttonColors(Colors.Blue),
                        modifier = Modifier
                                .height(48.dp)
                                .padding(horizontal = 6.dp)
                ) {
                    Text(
                            text = "Change Information",
                            color = Color.White
                    )
                }

            Spacer(modifier = Modifier.height(10.dp))
            Button(
                    onClick = { MainActivity.idPlayerConnected = -1
                        navController.navigate("home") },
                    shape = RoundedCornerShape(15),
                    colors = ButtonDefaults.buttonColors(Colors.Blue),
                    modifier = Modifier
                            .height(48.dp)
                            .padding(horizontal = 6.dp)
            ) {
                Text(
                        text = "disconnection",
                        color = Color.White
                )
            }

            Spacer(modifier = Modifier.height(10.dp))
            Button(
                    onClick = { confirmDialog = true },
                    shape = RoundedCornerShape(15),
                    colors = ButtonDefaults.buttonColors(Colors.Red),
                    modifier = Modifier
                            .height(48.dp)
                            .padding(horizontal = 16.dp)
            ) {
                Text(
                        text = "Delete Account",
                        color = Color.White
                )
            }
            if (showDialog) {
                if (player != null) {
                    ChangeDialog(player.id.toString(),player.nickname,onDismiss = { showDialog = false })
                }
            }
            if (confirmDialog) {
                if (player != null) {
                    DeleteConfirmDialog(onConfirmDelete = { ControllerPlayer.deletePlayer(player.id.toString())
                        MainActivity.idPlayerConnected = -1
                        navController.navigate("home")
                        confirmDialog = false},onDismiss = { confirmDialog = false })
                }
            }
    }
}
}

@Composable
fun ChangeDialog(playerId: String, currentNickname: String, onDismiss: () -> Unit) {

    var nickname by rememberSaveable { mutableStateOf(currentNickname) }
    var password by rememberSaveable { mutableStateOf("") }

    var nicknameError by remember { mutableStateOf(false) }
    var passwordError by remember { mutableStateOf(false) }

    AlertDialog(
            onDismissRequest = {
                onDismiss()
            },
            title = { Text("Change Information") },
            confirmButton = {
                Button(
                        colors = ButtonDefaults.buttonColors(Colors.Cyan),
                        onClick = {
                            if (nickname.isNotBlank() && password.isNotBlank()) {
                                val hashedPassword = BCrypt.hashpw(password, BCrypt.gensalt())
                                val formDataBuilder = MultipartBody.Builder().setType(MultipartBody.FORM)
                                formDataBuilder.addFormDataPart("nickname", nickname)
                                formDataBuilder.addFormDataPart("password", hashedPassword)
                                ControllerPlayer.updatePlayer(playerId, formDataBuilder)
                                onDismiss()
                            } else {
                                if (nickname.isBlank()) nicknameError = true
                                if (password.isBlank()) passwordError = true
                            }
                        }
                ) {
                    Text("Save")
                }
            },
            dismissButton = {
                Button(
                        colors = ButtonDefaults.buttonColors(Colors.BlackGrey),
                        onClick = {
                            onDismiss()
                        }
                ) {
                    Text("Dismiss")
                }
            },
            text = {
                Column(
                        modifier = Modifier
                                .fillMaxWidth()
                                .padding(8.dp)
                ) {
                    OutlinedTextField(
                            value = nickname,
                            onValueChange = {
                                nickname = it
                                nicknameError = false
                            },
                            label = { Text("Nickname") },
                            modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(8.dp),
                            isError = nicknameError
                    )
                    OutlinedTextField(
                            value = password,
                            onValueChange = {
                                password = it
                                passwordError = false
                            },
                            label = { Text("Password") },
                            visualTransformation = PasswordVisualTransformation(),
                            keyboardOptions = KeyboardOptions.Default.copy(
                                    keyboardType = KeyboardType.Password
                            ),
                            modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(8.dp),
                            isError = passwordError
                    )

                    if (nicknameError || passwordError) {
                        Text(
                                text = "Please fill in all fields",
                                color = Color.Red,
                                modifier = Modifier.padding(start = 8.dp)
                        )
                    }
                }
            }
    )
}


@Composable
fun DeleteConfirmDialog(onConfirmDelete: () -> Unit, onDismiss: () -> Unit) {
    AlertDialog(
            onDismissRequest = onDismiss,
            title = { Text("Confirm deleting account") },
            confirmButton = {
                Button(
                        onClick = {
                            onConfirmDelete()
                        },
                        colors = ButtonDefaults.buttonColors(Colors.Red)
                ) {
                    Text("Delete",color = Color.White)
                }
            },
            dismissButton = {
                Button(
                        onClick = {
                            onDismiss()
                        },
                        colors = ButtonDefaults.buttonColors(Color.LightGray)
                ) {
                    Text("Cancel",color = Color.White)
                }
            },
            text = {
                Text("Are you sure you want to delete your account?")
            }
    )
}